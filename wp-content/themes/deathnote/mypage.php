<?php
/*
  Template Name: My Page
*/
if( !session_id()){
 session_start();
}
if(!get_current_user_id()){
  wp_safe_redirect(get_bloginfo('url').'/');
  exit;
}
if(isset($_POST['update'])) {
  $result = apply_filters('result_mypage',$_POST);
}
get_header();
?>
<body class="home">
  <div class="background-cover"></div>
  <?php get_template_part('template/template','popup')?>
  <?php get_template_part('template/template','mobile')?>
  <div class="grid_1200 boxed" id="wrap">
    <?php get_template_part('template/template','header')?>
    <?php get_template_part('template/top/top','afterheader')?>
    <!-- End section-warp -->
    <div class="index-no-box"></div>
    <?php get_template_part('template/template','search')?>
    <!-- End section-warp -->
    <div class="clearfix"></div>
    <section class="container main-content page-right-sidebar">
      <div class="row">
        <div class="with-sidebar-container">
          <div class="main-sidebar-container col-md-9">
            <?php 
              $user = new WP_User(get_current_user_id());
            ?>
            <div class="page-content">
              <h2>
                契約者 <a href="#"><?php _e($user->user_nicename)?></a>
              </h2>
              <div class="user-profile-img">
                <a original-title="<?php _e($user->user_nicename)?>" class="tooltip-n" href="#">
                  <?php if(!get_user_meta(get_current_user_id(),'avatar',true)) : ?>
                  <img alt="<?php _e($user->user_nicename)?>" src="https://secure.gravatar.com/avatar/5c46ccfefa4618e30c9b5cc86f64f547?s=79&amp;d=mm&amp;r=g" srcset="https://secure.gravatar.com/avatar/5c46ccfefa4618e30c9b5cc86f64f547?s=158&amp;d=mm&amp;r=g 2x" class="avatar avatar-79 photo" height="79" width="79">
                  <?php else :?>
                    <img alt="<?php _e($user->user_nicename)?>" src="<?php _e(get_user_meta(get_current_user_id(),'avatar',true)['url'])?>" class="avatar avatar-79 photo" height="79" width="79">
                  <?php endif;?>
                </a>
              </div>
              <div class="ul_list ul_list-icon-ok about-user">
                <ul>
                  <li><i class="icon-plus"></i><strong>契約日: </strong><span><?php _e($user->user_registered)?></span></li>
                  <li><i class="icon-plus"></i><strong>契約者ID: </strong><span><?php _e($user->user_login)?></span></li>
                </ul>
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- End page-content -->
            <?php if(flash('notice')):?>
              <?php flash('notice') ?>
            <?php endif?>
            <div class="page-content">
              <div class="boxedtitle page-title">
                <h2>契約者情報を編集する</h2>
              </div>
              <div class="form-style form-style-4">
                
                <form class="edit-profile-form vpanel_form" method="post" enctype="multipart/form-data">
                  <div class="form-inputs clearfix">
                    <p>
                      <label>表示する名前</label>
                      <input name="user_name" id="display_name" type="text" value="<?php _e($user->user_nicename)?>">
                    </p>
                    <p>
                      <label for="email" class="required">メールアドレス<span>*</span></label>
                      <input name="email" id="email" type="email" value="<?php _e($user->user_email)?>">
                    </p>
                    <p>
                      <label for="newpassword" class="required">パスワード<span>*</span></label>
                      <input name="pass1" id="newpassword" type="password" value="">
                    </p>
                    <p>
                      <label for="newpassword2" class="required">パスワードの確認<span>*</span></label>
                      <input name="pass2" id="newpassword2" type="password" value="">
                    </p>
                  </div>
                  <div class="form-style form-style-2 form-style-3">
                    <label for="you_avatar">契約者の写真</label>
                    <div class="fileinputs">
                      <input type="file" name="you_avatar" id="you_avatar" value="">
                      <div class="fakefile">
                        <button type="button" class="small margin_0">ファイルを選択</button>
                        <span><i class="icon-arrow-up"></i>参照する</span>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <p class="form-submit">
                    <input type="submit" value="保存" name="update" class="button color small login-submit submit">
                  </p>
                </form>
              </div>
              <!-- End page-content -->
            </div>
            <!-- End main -->
            <div class="clearfix"></div>
          </div>
          <!-- End main -->
          <?php get_template_part('template/top/top','right')?>
          <!-- End sidebar -->
          <div class="clearfix">
          </div>
        </div>
        <!-- End with-sidebar-container -->
      </div>
      <!-- End row -->
    </section>
    <!-- End container -->
    <?php get_template_part('template/template','footer')?>
  </div>
  <!-- End wrap -->
<?php get_footer();?>