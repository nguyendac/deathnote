jQuery(document).ready(function($){
  $(document).on('click','.question_vote_up',function(e){
    e.preventDefault();
    var postId = $(this).data().deathnote;
    var vote = $(this);
    $.ajax({
      type: 'POST',
      url: admin_url,
      data: {
        action: 'hanlinglike',
        deathnote_id: postId
      },
      success: function(data,textStatus,XMLHttpRequest) {
        if(!data) {
          vote.closest('.question-inner').find(".no_vote_more").hide(10).text('いいね！は会員限定の機能です').slideDown(300).delay(1200).hide(300);
        } else {
          if(data == 2) {
            vote.closest('.question-inner').find(".no_vote_more").hide(10).text('２回以上 いいね！はできません').slideDown(300).delay(1200).hide(300);
          }
          if(data == 1) {
            var likes =  parseInt(vote.closest('.question-inner').find('.question_vote_result').text());
            vote.closest('.question-inner').find('.question_vote_result').text(likes+1);
          }
        }
      }
    });
  })

  //
  $(document).on('click','.comment_vote_up',function(e){
    e.preventDefault();
    var comment_id = $(this).data().comment;
    var vote = $(this);
    $.ajax({
      type: 'POST',
      url: admin_url,
      data: {
        action: 'hanlinglikecomment',
        comment_id: comment_id
      },
      success: function(data,textStatus,XMLHttpRequest) {
        if(!data) {
          vote.closest('.comment-text').find(".no_vote_more").hide(10).text('いいね！は会員限定の機能です').slideDown(300).delay(1200).hide(300);
        } else {
          if(data == 2) {
            vote.closest('.comment-text').find(".no_vote_more").hide(10).text('２回以上 いいね！はできません').slideDown(300).delay(1200).hide(300);
          }
          if(data == 1) {
            var likes =  parseInt(vote.closest('.comment-text').find('.question_vote_result').text());
            vote.closest('.comment-text').find('.question_vote_result').text(likes+1);
          }
        }
      }
    });
  })
  jQuery(".live-search").length && jQuery(".live-search").each(function() {
      var e, r = jQuery(this);
      r.on("keyup", function() {
          var t = (r = jQuery(this)).val();
          if ("" == t) r.parent().find(".search-results").addClass("results-empty").html("").hide();
          else {
              var n = r.parent().find(".search_type").val();
              void 0 !== n && !1 !== n || (n = r.parent().parent().parent().find(".search_type").val());
              var a = r.parent().find(".search_loader");
              clearTimeout(e), e = setTimeout(function() {
                  r.hasClass("header-live-search") ? jQuery(".header-search button i").attr("class", "fa fa-refresh fa-spin") : r.hasClass("breadcrumbs-live-search") ? jQuery(".search-input-form button i").attr("class", "fa fa-refresh fa-spin") : r.hasClass("live-search-big") ? r.parent().find("i").attr("class", "fa fa-refresh fa-spin") : a.show(10), jQuery.ajax({
                      url: admin_url,
                      type: "POST",
                      data: {
                          action: "ask_live_search",
                          search_value: t,
                          search_type: n
                      },
                      success: function(e) {
                          r.parent().find(".search-results").removeClass("results-empty").html(e).slideDown(300), r.hasClass("header-live-search") ? jQuery(".header-search button i").attr("class", "fa fa-search") : r.hasClass("breadcrumbs-live-search") ? jQuery(".search-input-form button i").attr("class", "fa fa-search") : r.hasClass("live-search-big") ? r.parent().find("i").attr("class", "fa fa-search") : a.hide(10)
                      }
                  })
              }, 500)
          }
      }), r.on("focus", function() {
          var e = jQuery(this);
          0 == e.parent().find(".results-empty").length && e.parent().find(".search-results").show()
      }), jQuery(".search_type").change(function() {
          0 == jQuery(this).parent().parent().parent().find(".results-empty").length && jQuery(this).parent().parent().parent().find(".search-results").addClass("results-empty").html("").hide()
      });
      var t = r.parent().find(".search-results"),
          n = r.get(0);
      jQuery("body").bind("click", function(e) {
          jQuery.contains(t.get(0), e.target) || e.target == n || t.hide()
      })
  })
})