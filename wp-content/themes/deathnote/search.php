<?php
/*
  Template Name: Search Page
 */

if( !session_id()){
  session_start();
}
get_header();
?>
<body>
  <div class="background-cover"></div>
  <?php get_template_part('template/template','popup')?>
  <?php get_template_part('template/template','mobile')?>
  <div id="wrap" class="grid_1200 boxed">
    <?php get_template_part('template/template','header')?>
    <div class="breadcrumbs">
      <section class="container">
        <div class="row">
          <div class="col-md-12">
            <h1>デスノートを拾う(ログインor登録)</h1>
            <div class="clearfix"></div>
            <div class="crumbs">
              <a itemprop="breadcrumb" href="/">トップページ</a><span class="crumbs-span">/</span> <span class="current">デスノートを拾う(ログインor登録)</span>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class='index-no-box'></div>
    <?php get_template_part('template/template','search')?>
    <!-- End section-warp -->
    <div class="clearfix"></div>
    <section class="container main-content page-right-sidebar">
      <div class="row">
        <div class="with-sidebar-container">
          <?php get_template_part('template/template','result')?>
          <!-- End main -->
          <?php get_template_part('template/top/top','right')?>
          <!-- End sidebar -->
          <div class="clearfix">
          </div>
        </div>
        <!-- End with-sidebar-container -->
      </div>
      <!-- End row -->
    </section>
    <?php get_template_part('template/top/top','adver')?>
    <div class="clearfix"></div>
    <!-- End container -->
    <?php get_template_part('template/template','footer')?>
  </div>
<?php get_footer();?>