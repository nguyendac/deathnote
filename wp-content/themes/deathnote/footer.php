<div class="go-up">
  <i class="icon-chevron-up">
  </i>
</div>
<script src="<?php bloginfo('template_url')?>/assets/js/tabs.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url')?>/assets/js/jquery.tipsy.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url')?>/assets/js/tags.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url')?>/assets/js/mCustomScrollbar.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url')?>/assets/js/custom.min.js" type="text/javascript"></script>
<?php wp_footer();?>
</body>
</html>