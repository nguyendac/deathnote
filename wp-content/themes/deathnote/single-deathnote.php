<?php
// for post type deathnote
if (have_posts()) : while (have_posts()) : the_post();
postview_set($post->ID);
get_header();
?>
<body>
  <div class="background-cover"></div>
  <?php get_template_part('template/template','popup')?>
  <?php get_template_part('template/template','mobile')?>
  <div id="wrap" class="grid_1200 boxed">
    <?php get_template_part('template/template','header')?>
    <div class="breadcrumbs">
      <section class="container">
        <div class="row">
          <div class="col-md-12">
            <h1>死神の声(ブログ)</h1>
            <div class="clearfix"></div>
            <div class="crumbs">
              <a itemprop="breadcrumb" href="index.html">トップページ</a><span class="crumbs-span">/</span> <span class="current">死神の声(ブログ)</span>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class='index-no-box'></div>
    <?php get_template_part('template/template','search')?>
    <div class="clearfix"></div>
    <div class="advertising">
      <div class="after-header">
        <ul>
          <li>
            2018.6.5
            <br>
            <a href="#">
            【時給2,000円・週1・3H・女性限定】東京都杉並区でベビーシッターを募集します
            </a>
            </br>
          </li>
        </ul>
      </div>
    </div>
    <!-- End advertising -->
    <div class="clearfix"></div>
    <section class="container main-content page-right-sidebar">
      <div class="row">
        <div class="with-sidebar-container">
          <div class="main-sidebar-container col-md-9">
            <div id="question-29248">
              <article class="question single-question question-type-normal no_reports post-29248 type-question status-publish hentry question-category-danna-ni-shindehoshii" id="post-29248">
                <h2><span itemprop="name"><?php the_title();?></span></h2>
                <div class="question-type-main"><i class="icon-question-sign"></i>デス書き込み</div>
                <div class="question-inner">
                  <div class="clearfix"></div>
                  <div class="question-desc">
                    <div class="content-text" itemprop="text">
                      <p><?php _e(nl2br(get_post_meta($post->ID,'content_deathnote',true)))?></p>
                      <?php get_template_part('template/deathnote/deathnote','social')?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="loader_2"></div>
                    <div class="clearfix"></div>
                    <div class="no_vote_more"></div>
                  </div>
                  <span style="display:block; float:left; margin-right:20px;"><i class="fa fa-tag"></i><?php _e($post->ID)?></span>
                  <?php
                    $terms = wp_get_post_terms($post->ID,'type',array("fields" => "all"));
                  ?>
                  <span class="question-category"><i class="fa fa-folder-o"></i><a href="<?php echo get_term_link($terms[0]) ?>" rel="tag"><?php _e($terms[0]->name)?></a></span>
                  <span class="question-author-meta">
                  <a href=""><i class="icon-user"></i><?php the_author_meta('user_nicename',$post->post_author) ?></a>
                  </span>
                  <span class="question-date"><i class="fa fa-calendar"></i><?php _e(get_the_date('Y/m/d(月) H:i'))?></span>
                  <span class="question-comment"><a href=""><i class="fa fa-comments-o"></i><span itemprop="answerCount"><?php echo wp_count_comments($post->ID)->total_comments; ?></span> デスコメント</a></span>
                  <span class="question-view"><i class="icon-eye-open"></i><?php _e(postview_get($post->ID))?> アクセス</span>
                  <span itemprop="upvoteCount" class="single-question-vote-result question_vote_result"><?php _e(getlike($post->ID)) ?></span>
                  <ul class="single-question-vote">
                    <li><a href="#" data-deathnote="<?php _e($post->ID)?>"  class="single-question-vote-up ask_vote_up question_vote_up vote_not_user tooltip_s" original-title="いいね！"><i class="icon-thumbs-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
              </article>
              <div class="clearfix"></div>
              <div class="advertising">
                <!-- ■ 記事ページの記事下 -->
                <div class="pc">
                  <a href="http://clk-fm-report.net/?id=336" rel="nofollow" title="ぴゅあじょ"><img src=""></a>
                </div>
                <div class="sp">
                  <a href="http://clk-fm-report.net/?id=335" rel="nofollow" title="ぴゅあじょ"><img src=""></a>
                </div>
                <!-- ■ 記事ページの記事下 -->
              </div>
              <!-- End advertising -->
              <div class="clearfix"></div>
              <div itemprop="author" itemscope="" itemtype="http://schema.org/Person" class="about-author clearfix">
                <span itemprop="name" class="hide"><?php the_author_meta('user_nicename',$post->post_author) ?></span>
                <div class="author-image">
                  <a href="" original-title="<?php the_author_meta('user_nicename',$post->post_author) ?>" class="tooltip-n">
                    <?php if(!get_user_meta($post->post_author,'avatar',true)) : ?>
                    <img alt="<?php the_author_meta('user_nicename',$post->post_author) ?>" src="https://secure.gravatar.com/avatar/2c5a75f3b77f70f607630a86762a4a8e?s=65&amp;d=mm&amp;r=g" srcset="https://secure.gravatar.com/avatar/2c5a75f3b77f70f607630a86762a4a8e?s=130&amp;d=mm&amp;r=g 2x" class="avatar avatar-65 photo" height="65" width="65">
                    <?php else :?>
                      <img alt="<?php the_author_meta('user_nicename',$post->post_author) ?>" src="<?php _e(get_user_meta($post->post_author,'avatar',true)['url'])?>" class="avatar avatar-79 photo" height="79" width="79">
                    <?php endif;?>
                  </a>
                </div>
                <div class="author-bio">
                  <h4>契約者 <a href=""><?php the_author_meta('user_nicename',$post->post_author) ?></a></h4>
                  <div class="clearfix"></div>
                </div>
              </div>
              <!-- End about-author -->
              <?php get_template_part('template/deathnote/deathnote','related')?>
              <!-- End related-posts -->
              <div class="clearfix"></div>
              <div class="advertising">
                <!-- ■ 記事ページ関連下 -->
                <div>
                  <a href=""><img src="" title="だんなデス・ノート本"></a>
                </div>
                <!-- ■ 記事ページ関連下 -->
              </div>
              <!-- End advertising -->
              <div class="clearfix"></div>
              <div id="comments"></div>
              <div class="page-content">

                  <?php
                    $comments = get_comments(
                      array(
                        'post_id'=>$post->ID,
                      )
                    );
                  ?>
                  <?php showComments($comments,$parent_id = 0,$stt = 0,$post->ID);?>

              </div>
              <div class="page-content clearfix no_comment_box">
                <?php
                  $comments_args = array(
                    'fields'=> array(
                      'author' => '<div id="respond-inputs" class="clearfix"><p><label for="comment_name">お名前</label><input name="author" type="text" value="" id="comment_name" aria-required="true"></p></div>',
                    ),
                    'title_reply_before' => '<div class="boxedtitle page-title">',
                    'title_reply' => '<h2>コメント</h2>',
                    'title_reply_to' => '',
                    'cancel_reply_link' => '',
                    'title_reply_after' => '</div>',
                    'cancel_reply_before'=> '',
                    'cancel_reply_after' => '',
                    'comment_notes_before' => '',
                    'comment_notes_after'  => '',
                    'comment_field' => '<div id="respond-textarea"><p><label class="required" for="comment">コメント<span>*</span></label><textarea id="comment" name="comment" aria-required="true" cols="58" rows="10"></textarea></p></div>',
                    'label_submit' =>  'コメントをする',
                    'logged_in_as' => '<p class="logged-in-as">' . sprintf( __( 'ログイン中 <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>'
                  );
                  comment_form($comments_args);
                ?>
              </div>
              <div class="post-next-prev clearfix">
                <p class="prev-post">
                  <?php previous_post_link('%link','<i class="icon-double-angle-left"></i>&nbsp;前のデス書き込み',false); ?>
                </p>
                <p class="next-post">
                  <?php next_post_link('%link','<i class="icon-double-angle-right"></i>次のデス書き込み&nbsp;',false); ?>
                </p>
              </div>
              <!-- End post-next-prev -->
            </div>
            <div class="clearfix"></div>
            <div class="advertising">
              <!-- ■ 全ページ最下部 -->
              <div class="pc">
                <a href="http://cocotera.jp/lp/" rel="nofollow" title="こころテラス"><img src=""></a>
              </div>
              <div class="sp">
                <a href="http://www.tantei-mr.co.jp/lp/uwaki/61.html" rel="nofollow" title="総合探偵社MR"><img src=""></a>
              </div>
              <!-- ■ 全ページ最下部 -->
            </div>
            <!-- End advertising -->
            <div class="clearfix"></div>
          </div>
          <?php get_template_part('template/template','sidebar')?>
          <div class="clearfix"></div>
        </div>
        <!-- End with-sidebar-container -->
      </div>
      <!-- End row -->
    </section>
    <?php get_template_part('template/template','footer')?>
  </div>
<?php get_footer();?>
<?php endwhile; endif; ?>