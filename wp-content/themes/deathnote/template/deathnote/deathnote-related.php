<div id="related-posts">
  <h2><?php _e('関連するデス書き込み','deathnote')?></h2>
  <?php
    $terms = wp_get_post_terms($post->ID,'type',array("fields" => "all"));
    $relates = apply_filters('related_deathnote',$post->ID,$terms[0]->slug);
  ?>
  <ul class="related-posts">
    <?php while($relates->have_posts()):$relates->the_post(); ?>
    <li class="related-item">
      <h3><a href="<?php the_permalink()?>" title="「俺が何したっていうんだよ！」これが、旦那の決めゼリフ！"><i class="icon-double-angle-right"></i><?php the_title();?></a></h3>
    </li>
  <?php endwhile;wp_reset_query();?>
  </ul>
</div>