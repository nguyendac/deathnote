<div class="col-md-6">
  <div class="page-content">
    <h2>デスノートを取り出す(ログイン)</h2>
    <div class="form-style form-style-3">
      <div class="ask_form inputs">
        <form class="login-form ask_login" action="" method="post">
          <div class="ask_error"></div>
          <div class="form-inputs clearfix">
            <?php if(flash('login_success')):?>
              <?php flash('login_success') ?>
            <?php endif?>
            <?php if(flash('login_fail')):?>
              <?php flash('login_fail') ?>
            <?php endif?>
            <p class="login-text">
              <input class="required-item" type="text" value="契約者ID( 英数字のみ )" onfocus="if (this.value == '契約者ID( 英数字のみ )') {this.value = '';}" onblur="if (this.value == '') {this.value = '契約者ID( 英数字のみ )';}" name="log">
              <i class="icon-user"></i>
            </p>
            <p class="login-password">
              <input class="required-item" type="password" value="パスワード" onfocus="if (this.value == 'パスワード') {this.value = '';}" onblur="if (this.value == '') {this.value = 'パスワード';}" name="pwd">
              <i class="icon-lock"></i>
            </p>
          </div>
          <p class="form-submit login-submit">
            <span class="loader_2"></span>
            <input type="submit" value="デスノートを取り出す(ログイン)" name="login" class="button color small login-submit submit sidebar_submit">
          </p>
          <div class="rememberme">
            <label><input type="checkbox"input name="rememberme" checked="checked"> 記憶する</label>
          </div>
          <div class="errorlogin"></div>
        </form>
      </div>
    </div>
  </div>
  <!-- End page-content -->
</div>
<!-- End col-md-6 -->