<div class="col-md-6">
  <div class="tabs-warp tabs-register-forget">
    <ul class="tabs">
      <li class="tab"><a href="#"><?php _e('デスノートを拾う(無料登録)','deathnote')?></a></li>
      <li class="tab"><a href="#"><?php _e('パスワードを忘れた','deathnote')?></a></li>
    </ul>
    <div class="tab-inner-warp">
      <div class="tab-inner">
        <div class="form-style form-style-3">
          <form method="post" class="signup_form ask_form" enctype="multipart/form-data" id="login-vs-register" action="">
            <div class="form-inputs clearfix">
              <?php if(flash('required')):?>
                <?php flash('required') ?>
              <?php endif?>
              <?php if(flash('unique')):?>
                <?php flash('unique') ?>
              <?php endif?>
              <?php if(flash('password')):?>
                <?php flash('password') ?>
              <?php endif?>
              <?php if(flash('success')):?>
                <?php flash('success') ?>
              <?php endif?>
              <p>
                <label for="user_name_345" class="required"><?php _e('契約者ID( 英数字のみ )','deathnote')?><span>*</span></label>
                <input type="text" class="required-item_news" name="user_name" id="user_name_345" value="">

              </p>
              <p>
                <label for="email_345" class="required"><?php _e('メールアドレス','deathnote')?><span>*</span></label>
                <input type="email" class="required-item_news" name="email" id="email_345" value="">
              </p>
              <p>
                <label for="pass1_345" class="required"><?php _e('パスワード','deathnote')?><span>*</span></label>
                <input type="password" class="required-item_news" name="pass1" id="pass1_345" autocomplete="off">
              </p>
              <p>
                <label for="pass2_345" class="required"><?php _e('パスワードの確認','deathnote')?><span>*</span></label>
                <input type="password" class="required-item_news" name="pass2" id="pass2_345" autocomplete="off">

              </p>
              <!-- <p class='ask_captcha_p'>
                <label for='ask_captcha_345' class='required'><?php _e('画像の文字を入力','deathnote')?><span>*</span></label>
                <input size='10' id='ask_captcha_345' name='ask_captcha' class='ask_captcha' value='' type='text'><img class='ask_captcha_img' src='https://danna-shine.com/cms/wp-content/themes/danna-shine_53of/captcha/create_image.php' alt='画像の文字を入力' title='文字が見づらい場合は画像をクリックしてください' onclick="javascript:ask_get_captcha('https://danna-shine.com/cms/wp-content/themes/danna-shine_53of/captcha/create_image.php', 'ask_captcha_img_345');" id='ask_captcha_img_345'>
                <span class='question_poll ask_captcha_span'>クリックすると違う画像が表示されます</span>
              </p> -->
            </div>
            <p class="form-submit">
              <!-- <input type="hidden" name="redirect_to" value="https://danna-shine.com/login-or-register"> -->
              <input type="submit" name="register" value="覚悟を持ってデスノートを拾う( 登録 )" class="button color dark_button small submit">
              <!-- <input type="hidden" name="form_type" value="ask-signup"> -->
            </p>
          </form>
        </div>
      </div>
    </div>
    <div class="tab-inner-warp">
      <div class="tab-inner">
        <div class="form-style form-style-3">
          <form method="post" class="ask-lost-password ask_form" action="">
            <div class="ask_error"></div>
            <div class="form-inputs clearfix">
              <p>
                <label for="user_mail_984" class="required">メールアドレス<span>*</span></label>
                <input type="email" class="required-item_news" name="user_mail" id="user_mail_984">
              </p>
            </div>
            <p class="form-submit">
              <input type="submit" value="リセット" class="button color dark_button small submit">
              <input type="hidden" name="form_type" value="ask-forget">
            </p>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End col-md-6 -->