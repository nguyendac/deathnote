<aside class="mobile-aside mobile-menu-wrap mobile-login-wrap dark-mobile-menu panel_dark">
  <div class="mobile-aside-inner">
    <div class="mobile-aside-inner-inner">
      <a class="mobile-aside-close" href="#">x</a>
      <div class="row">
        <div class="col-md-6">
          <div class="page-content">
            <h2>
              デスノートを取り出す(ログイン)
            </h2>
            <div class="form-style form-style-3">
              <div class="ask_form inputs">
                <form action="" class="login-form ask_login" method="post">
                  <div class="ask_error">
                  </div>
                  <div class="form-inputs clearfix">
                    <p class="login-text">
                      <input class="required-item" name="log" onblur="if (this.value == '') {this.value = '契約者ID( 英数字のみ )';}" onfocus="if (this.value == '契約者ID( 英数字のみ )') {this.value = '';}" type="text" value="契約者ID( 英数字のみ )">
                      <i class="icon-user">
                      </i>
                      </input>
                    </p>
                    <p class="login-password">
                      <input class="required-item" name="pwd" onblur="if (this.value == '') {this.value = 'パスワード';}" onfocus="if (this.value == 'パスワード') {this.value = '';}" type="password" value="パスワード">
                      <i class="icon-lock">
                      </i>
                      <a href="#">
                      パスワードを忘れた
                      </a>
                      </input>
                    </p>
                  </div>
                  <p class="form-submit login-submit">
                    <span class="loader_2">
                    </span>
                    <input class="button color small login-submit submit sidebar_submit" type="submit" value="デスノートを取り出す(ログイン)">
                    </input>
                  </p>
                  <div class="rememberme">
                    <label>
                    <input checked="checked" input="" name="rememberme" type="checkbox">
                    記憶する
                    </input>
                    </label>
                  </div>
                  <input name="redirect_to" type="hidden" value="">
                  <input name="login_nonce" type="hidden" value="ef2b80ca61">
                  <input name="ajax_url" type="hidden" value="">
                  <input name="form_type" type="hidden" value="ask-login">
                  <div class="errorlogin">
                  </div>
                  </input>
                  </input>
                  </input>
                  </input>
                </form>
              </div>
            </div>
          </div>
          <!-- End page-content -->
        </div>
        <!-- End col-md-6 -->
        <div class="col-md-6">
          <div class="page-content Register">
            <h2>
              デスノートを拾う(無料登録)
            </h2>
            <p>
              よぅ、死神だ。だんなDEATH NOTEを書くにはデスノートを拾う必要がある。さぁ、名前とメールアドレスを入力して、デスノートを拾え。これでようやくクズ旦那とお別れできる・・・・かもな。
            </p>
            <div class="button color small signup">
              デスノートを拾う(無料登録)
            </div>
          </div>
          <!-- End page-content -->
        </div>
        <!-- End col-md-6 -->
      </div>
    </div>
    <!-- End mobile-aside-inner-inner -->
  </div>
  <!-- End mobile-aside-inner -->
</aside>
<!-- End mobile-aside -->
<aside class="mobile-aside mobile-menu-wrap aside-no-cart dark-mobile-menu">
  <div class="mobile-aside-inner">
    <div class="mobile-aside-inner-inner">
      <a class="mobile-aside-close" href="#">x</a>
      <div class="mobile-menu-top mobile-aside-menu">
        <div class="header-top">
          <ul class="menu_aside">
            <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-home menu-item-5909" id="menu-item-5909">
              <a class="" href="#" title="ホーム">
              <i class="icon-home">
              </i>
              </a>
            </li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7018" id="menu-item-7018">
              <a class="" href="login_or_register.html" title="デスノートを取り出す(ログイン)">
              <i class="icon-signin">
              </i>
              デスノートを取り出す(ログイン)
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="ask-question-menu">
        <a class="color button small margin_0" href="add-deathnote.html">
        デスノートを書く
        </a>
      </div>
      <!-- End ask-question-menu -->
      <div class="post-search">
        <form action="" class="searchform" method="get" role="search">
          <div class="row">
            <div class="col-md-8">
              <div class="mobile-search-result">
                <input class="live-search" autocomplete="off" type="search" name="search" value="キーワード入力" onfocus="if(this.value=='キーワード入力')this.value='';" onblur="if(this.value=='')this.value='キーワード入力';">
                <input name="page_id" type="hidden" value="5687">
                <input name="search_type" type="hidden" value="questions">
                <div class="loader_2 search_loader"></div>
                <div class="search-results results-empty"></div>
                </input>
                </input>
                </input>
              </div>
            </div>
            <!-- End col-md-8 -->
            <div class="col-md-4">
              <input class="button-default" type="submit" value="検索">
              </input>
            </div>
            <!-- End col-md-4 -->
          </div>
          <!-- End row -->
        </form>
      </div>
      <div class="mobile-menu-left mobile-aside-menu">
        <div class="header-menu">
          <ul class="menu_aside">
            <li class="menu-item menu-item-type-post_type menu-item-object-page">
              <a class="" href="hadd-deathnote.html">
              デスノートを書く
              </a>
            </li>
            <li class="menu-item menu-item-type-custom menu-item-object-custom">
              <a class="" href="#">
              死神の声(ブログ)
              </a>
            </li>
          </ul>
        </div>
      </div>
      <!-- End mobile-menu-left -->
    </div>
    <!-- End mobile-aside-inner-inner -->
  </div>
  <!-- End mobile-aside-inner -->
</aside>
<!-- End mobile-aside -->