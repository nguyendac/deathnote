<aside class="col-md-3 sidebar">
  <div class="widget widget_nav_menu" id="nav_menu-6">
    <h3 class="widget_title">
      Ranking By View
    </h3>
    <div>
      <?php do_action('show_list_popular_post_side_bar') ?>
    </div>
  </div>
  <div class="widget widget_text" id="text-17">
    <h3 class="widget_title">
      Rank by like
    </h3>
    <div class="textwidget">
       <?php do_action('show_list_popular_post_like_side_bar') ?>
    </div>
  </div>
  <div class="widget questions_categories-widget" id="questions_categories-widget-2">
    <h3 class="widget_title">
      カテゴリ
    </h3>
    <ul>
      <li>
        <a href="#">だんなに死んでほしい<span>(<span>17936 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">だんなと離婚したい<span>(<span>1577 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">結婚直前の自分に教えたいこと<span>(<span>36 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">だんなに贈る川柳５・７・５<span>(<span>526 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">同じ状況の女性に聞いてみたい<span>(<span>322 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">密かな旦那への復讐<span>(<span>292 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">彼氏に死んで欲しい<span>(<span>257 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">だんなと再構築したい<span>(<span>155 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">旦那のせいで死にたい<span>(<span>87 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">わたしの汚料理レシピ<span>(<span>32 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">デスノート効果あり報告<span>(<span>52 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">旦那を許す<span>(<span>2 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">離婚して自立した経験談<span>(<span>26 デス書き込み</span>)</span></a>
      </li>
    </ul>
  </div>
</aside>