<div class="main-sidebar-container col-md-9">
  <div class="tabs-warp question-tab">
    <ul class="tabs not-tabs">
      <?php
        $tabs = 'recent_questions';
        if(array_key_exists('tabs',$_GET)) {
          $tabs = $_GET['tabs'];
        }
      ?>
      <li class="tab">
        <a class="<?php if($tabs == 'recent_questions'){ echo 'current';} ?>" data-js="recent_questions" href="?tabs=recent_questions">最新のデスノート▼</a>
      </li>
      <li class="tab">
        <a class="<?php if($tabs == 'recently_answered'){ echo 'current';} ?>" data-js="recently_answered" href="?tabs=recently_answered">最新のコメント▼</a>
      </li>
      <li class="tab">
        <a class="<?php if($tabs == 'most_vote'){ echo 'current';} ?>" data-js="most_vote" href="?tabs=most_vote">いいね！が多い▼</a>
      </li>
      <!-- <li class="tab">
        <a data-js="question_bump" href="#">デスノート効果あり報告</a>
      </li> -->
    </ul>
    <div class="tab-inner-warp">
      <div class="tab-inner">
        <?php if($tabs != 'recently_answered'):?>
        <?php
          $deathnotes = apply_filters('list_note',$_GET);
          while($deathnotes->have_posts()):$deathnotes->the_post();
        ?>
        <article class="question question-type-normal no_reports type-question">
          <h2>
            <a href="<?php the_permalink()?>">
              <span itemprop="name"><?php the_title()?></span>
            </a>
          </h2>
          <div class="question-type-main"><i class="icon-question-sign"></i>デス書き込み</div>
          <div class="question-author" itemprop="author" itemscope="">
            <span class="hide" itemprop="name"><?php the_author_meta('user_nicename',$post->post_author) ?></span>
            <a class="question-author-img tooltip-n" href="#" original-title="<?php the_author_meta('user_nicename',$post->post_author) ?>">
              <span></span>
              <?php if(!get_user_meta($post->post_author,'avatar',true)) : ?>
                <img alt="<?php the_author_meta('user_nicename',$post->post_author) ?>" class="avatar avatar-65 photo" height="65" src="https://secure.gravatar.com/avatar/345fbd70a4931ce3c0f6b85e583052a9?s=65&d=mm&r=g" srcset="https://secure.gravatar.com/avatar/345fbd70a4931ce3c0f6b85e583052a9?s=130&d=mm&r=g 2x" width="65"/>
              <?php else :?>
                <img alt="<?php the_author_meta('user_nicename',$post->post_author) ?>" src="<?php _e(get_user_meta($post->post_author,'avatar',true)['url'])?>" class="avatar avatar-79 photo" height="79" width="79">
              <?php endif;?>
            </a>
          </div>
          <div class="question-inner">
            <div class="clearfix"></div>
            <div class="question-desc">
              <div itemprop="text">
                <?php _e(get_post_meta($post->ID,'content_deathnote',true))?>
              </div>
              <div class="no_vote_more"></div>
            </div>
            <span style="display:block; float:left; margin-right:20px;"><i class="fa fa-tag"></i><?php _e($post->ID)?></span>
            <?php
            $terms = apply_filters('getterm',$post->ID);
            ?>
            <span class="question-category"><i class="fa fa-folder-o"></i><a href="<?php echo get_term_link($terms[0])?>" rel="tag"><?php _e($terms[0]->name)?></a></span>
            <span class="question-author-meta"></span>
            <span class="question-date"><i class="fa fa-calendar"></i><?php _e(get_the_date('Y/m/d(月) H:i'))?></span>
            <span class="question-comment">
              <a href="#"><i class="fa fa-comments-o"></i>
                <span itemprop="answerCount"><?php echo wp_count_comments($post->ID)->total_comments; ?></span>デスコメント
              </a>
            </span>
            <span class="question-view"><i class="icon-eye-open"></i><?php _e(postview_get(get_the_ID()))?> アクセス</span>
            <span class="single-question-vote-result question_vote_result" itemprop="upvoteCount"><?php _e(getlike($post->ID)) ?></span>
            <ul class="single-question-vote">
              <li>
                <a class="ask_vote_up question_vote_up vote_not_user tooltip_s" data-deathnote="<?php _e($post->ID)?>" href="#" original-title="いいね！"><i class="icon-thumbs-up"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
        </article>
        <?php endwhile;wp_reset_query();?>
        <div class="pagination">
          <?php
            mp_pagination($prev = '&lt;', $next = '&gt;', $pages=$deathnotes->max_num_pages);
            wp_reset_query();
          ?>
        </div>
      <?php else:?>
        <div id="commentlist" class="page-content">
          <ol class="commentlist clearfix">
            <?php
              $deaths = apply_filters('list_note',$_GET);
              if($deaths) :
                foreach($deaths as $death):
                  $comment = get_comment($death->last_comment_id);
            ?>
            <li rel="posts-1217" class="comment" id="comment-16034">
              <div class="comment-body clearfix" rel="post-1217" itemscope="" itemtype="http://schema.org/Answer">
                <h3><a href="<?php the_permalink($death->ID)?>"><?php _e($death->post_title)?></a></h3>
                <div class="avatar-img">
                    <?php if(!get_user_meta($comment->comment_author,'avatar',true)) : ?>
                    <img alt="<?php the_author_meta('user_nicename',$comment->comment_author) ?>" class="avatar avatar-65 photo" height="65" src="https://secure.gravatar.com/avatar/345fbd70a4931ce3c0f6b85e583052a9?s=65&d=mm&r=g" srcset="https://secure.gravatar.com/avatar/345fbd70a4931ce3c0f6b85e583052a9?s=130&d=mm&r=g 2x" width="65"/>
                  <?php else :?>
                    <img alt="<?php the_author_meta('user_nicename',$comment->comment_author) ?>" src="<?php _e(get_user_meta($comment->comment_author,'avatar',true)['url'])?>" class="avatar avatar-79 photo" height="79" width="79">
                  <?php endif;?>
                </div>
                <div class="comment-text">
                  <div class="author clearfix">
                    <div class="comment-author" itemprop="author" itemscope="" itemtype="http://schema.org/Person">
                      <span itemprop="name"><?php echo $comment->comment_author?></span></div>
                      <div class="comment-vote">
                        <ul class="single-question-vote">
                          <li class="loader_3"></li>
                          <li>
                            <a href="#" class="single-question-vote-up ask_vote_up comment_vote_up vote_allow" title="いいね！" data-comment="<?php echo $comment->comment_ID?>">
                              <i class="icon-thumbs-up"></i>
                            </a>
                          </li>
                        </ul>
                      </div>
                      <span itemprop="upvoteCount" class="question-vote-result question_vote_result"><?php _e(getlikecomment($comment->comment_ID)) ?></span>
                      <div class="comment-meta" itemprop="dateCreated" datetime="2018年7月3日">
                        <a href="<?php the_permalink($death->ID)?>" class="date"><i class="fa fa-calendar"></i><?php _e($comment->comment_date)?></a>
                      </div>
                      <div class="comment-reply"></div>
                    </div>
                  <div class="text">
                    <div itemprop="text">
                      <a href="<?php the_permalink($death->ID)?>"><?php _e($comment->comment_content)?></a>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="loader_3"></div>
                  <div class="no_vote_more"></div>
                </div>
              </div>
            </li>
          <?php endforeach;endif;?>
          </ol>
        </div>
        <div class="pagination">
          <?php
            $total = apply_filters('get_total_pc','');
            mp_pagination($prev = '&lt;', $next = '&gt;', $total);
            wp_reset_query();
          ?>
        </div>
      <?php endif;?>
      </div>
    </div>
  </div>
  <!-- End tabs-warp -->
  <div class="clearfix"></div>
  <div class="advertising">
    <!-- ■ 全ページ最下部 -->
    <div class="pc">
      <a href="https://www.tantei-mr.co.jp/lp/uwaki/61.html?t=2" rel="nofollow" title="総合探偵社MR">
      <img src="<?php bloginfo('template_url')?>/assets/images/2018-02-06_728x90.jpg"/>
      </a>
    </div>
    <div class="sp">
      <a href="http://www.tantei-mr.co.jp/lp/uwaki/61.html" rel="nofollow" title="総合探偵社MR">
      <img src="<?php bloginfo('template_url')?>/assets/images/320x100-1.png"/>
      </a>
    </div>
    <!-- ■ 全ページ最下部 -->
  </div>
  <!-- End advertising -->
  <div class="clearfix"></div>
</div>