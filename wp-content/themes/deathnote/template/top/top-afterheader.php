<div class="section-warp top-after-header" style="background-color:#0c0c0c;background-image:url(https://danna-shine.com/cms/wp-content/uploads/2017/07/2015-08-01-003b.jpg);background-repeat:no-repeat;background-attachment:scroll;background-position-x:center;background-position-y:top;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;">
  <div class="container clearfix">
    <div class="box_icon box_warp box_no_border box_no_background">
      <div class="row">
        <div class="col-md-12">
          <h2>
            夫に今すぐ
            <span class="accent">
            死んで欲しい
            </span>
            。毎日思っている。
          </h2>
          <p>
            結婚した自分があきれる。鬱になりそう。お願いだから死んで。世の中にはお前より素晴らしい人達が生きられないとか不公平だ。
          </p>
          <div class="clearfix">
          </div>
          <a class="color button dark_button medium" href="/login-vs-register">
          デスノートを拾う(新規登録)
          </a>
          <div class="clearfix">
            <a class="add_ntn" href="/add-deathnote"><span class="col-sm-12 color button large publish-question ask-not-login">
              投稿する
              </span></a>
          </div>
          
        </div>
      </div>
      <!-- End row -->
    </div>
    <!-- End box_icon -->
  </div>
  <!-- End container -->
</div>