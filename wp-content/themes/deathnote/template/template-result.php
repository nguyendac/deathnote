<div class="main-sidebar-container col-md-9">
  <div class="tabs-warp question-tab">
    <div class="tab-inner-warp">
      <div class="tab-inner">
        <?php
          $deathnotes = apply_filters('list_search',$_GET);
          while($deathnotes->have_posts()):$deathnotes->the_post();
        ?>
        <article class="question question-type-normal no_reports type-question status-publish hentry question-category-danna-ni-shindehoshii" >
          <h2>
            <a href="<?php the_permalink()?>">
              <span itemprop="name"><?php the_title();?></span>
            </a>
          </h2>
          <div class="question-type-main"><i class="icon-question-sign"></i>デス書き込み</div>
          <div class="question-author" itemprop="author" itemscope="">
            <span class="hide" itemprop="name"><?php the_author_meta('user_nicename',$post->post_author) ?></span>
            <a class="question-author-img tooltip-n" href="#" original-title="<?php the_author_meta('user_nicename',$post->post_author) ?>">
              <span></span>
              <?php if(!get_user_meta($post->post_author,'avatar',true)) : ?>
              <img alt="<?php the_author_meta('user_nicename',$post->post_author) ?>" src="https://secure.gravatar.com/avatar/2c5a75f3b77f70f607630a86762a4a8e?s=65&amp;d=mm&amp;r=g" srcset="https://secure.gravatar.com/avatar/2c5a75f3b77f70f607630a86762a4a8e?s=130&amp;d=mm&amp;r=g 2x" class="avatar avatar-65 photo" height="65" width="65">
              <?php else :?>
                <img alt="<?php the_author_meta('user_nicename',$post->post_author) ?>" src="<?php _e(get_user_meta($post->post_author,'avatar',true)['url'])?>" class="avatar avatar-79 photo" height="79" width="79">
              <?php endif;?>
            </a>
          </div>
          <div class="question-inner">
            <div class="clearfix"></div>
            <div class="question-desc">
              <div itemprop="text">
                <?php _e(get_post_meta($post->ID,'content_deathnote',true))?>
              </div>
              <div class="no_vote_more"></div>
            </div>
            <span style="display:block; float:left; margin-right:20px;"><i class="fa fa-tag"></i><?php _e($post->ID)?></span>
            <?php
            $terms = wp_get_post_terms($post->ID,'type',array("fields" => "all"));
            ?>
            <span class="question-category"><i class="fa fa-folder-o"></i><a href="<?php echo get_term_link($terms[0])?>" rel="tag"><?php _e($terms[0]->name)?></a></span>
            <span class="question-author-meta"></span>
            <span class="question-date"><i class="fa fa-calendar"></i><?php _e(get_the_date('Y/m/d(月) H:i'))?></span>
            <span class="question-comment">
              <a href="#"><i class="fa fa-comments-o"></i>
                <span itemprop="answerCount"><?php echo wp_count_comments($post->ID)->total_comments; ?></span>デスコメント
              </a>
            </span>
            <span class="question-view"><i class="icon-eye-open"></i><?php _e(postview_get(get_the_ID()))?> アクセス</span>
            <span class="single-question-vote-result question_vote_result" itemprop="upvoteCount"><?php _e(getlike($post->ID)) ?></span>
            <ul class="single-question-vote">
              <li>
                <a class="ask_vote_up question_vote_up vote_not_user tooltip_s" data-deathnote="<?php _e($post->ID)?>" href="#" original-title="いいね！"><i class="icon-thumbs-up"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
        </article>
        <?php endwhile;wp_reset_query();?>
        <div class="pagination">
          <?php
            mp_pagination($prev = '&lt;', $next = '&gt;', $pages=$deathnotes->max_num_pages);
            wp_reset_query();
          ?>
        </div>
      </div>
    </div>
  </div>
  <!-- End tabs-warp -->
  <div class="clearfix"></div>
  <div class="advertising">
    <!-- ■ 全ページ最下部 -->
    <div class="pc">
      <a href="https://www.tantei-mr.co.jp/lp/uwaki/61.html?t=2" rel="nofollow" title="総合探偵社MR">
      <img src="<?php bloginfo('template_url')?>/assets/images/2018-02-06_728x90.jpg"/>
      </a>
    </div>
    <div class="sp">
      <a href="http://www.tantei-mr.co.jp/lp/uwaki/61.html" rel="nofollow" title="総合探偵社MR">
      <img src="<?php bloginfo('template_url')?>/assets/images/320x100-1.png"/>
      </a>
    </div>
    <!-- ■ 全ページ最下部 -->
  </div>
  <!-- End advertising -->
  <div class="clearfix"></div>
</div>