<div class="section-warp top-after-header big-search">
  <div class="container clearfix">
    <div class="box_icon box_warp box_no_border box_no_background">
      <div class="row">
        <div class="col-md-12">
          <form class="form-style form-style-2" method="get" action="/search-title">
            <div class="search-p">
              <input class="live-search live-search-big" autocomplete="off" type="text" value="検索" onfocus="if(this.value=='検索')this.value='';" onblur="if(this.value=='')this.value='検索';" name="key">
              <i class="fa fa-search"></i>
              <button class="ask-search"><span class="color button small publish-question">検索</span></button>
              <div class="search-results results-empty" style="display: none;"></div>
            </div>
          </form>
        </div>
      </div>
      <!-- End row -->
    </div>
    <!-- End box_icon -->
  </div>
  <!-- End container -->
</div>