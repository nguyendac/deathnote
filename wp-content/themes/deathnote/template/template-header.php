<div id="header-top">
  <section class="container clearfix">
    <div class="row">
      <div class="col-md-6">
        <nav class="header-top-nav">
          <div class="header-top">
            <ul class="">
              <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-home menu-item-5909">
                <a class="" href="/" title="ホーム"><i class="icon-home"></i></a>
              </li>
              <?php if(get_current_user_id()):?>
                <li>
                  <a class="" href="/add-deathnote"><i class="fa fa-pencil-square-o"></i></a>
                </li>
                <li>
                  <a class="" title="" href="/my-page"><i class="icon-user"></i></a>
                </li>
                <!-- <li class="">
                  <a class="" href="/login-vs-register" title="デスノートを取り出す(ログイン)">
                  <i class="icon-signin"></i>
                  <?php _e('デスノートを取り出す(ログイン)','deathnote')?>
                  </a>
                </li> -->
                <li class="li-logout">
                  <a class="" href="<?php echo wp_logout_url('/'); ?>">デスノートをしまう<i class="icon-signout"></i></a>
                </li>
              <?php endif;?>
              <?php if(!get_current_user_id()):?>
                <li class="">
                  <a class="" href="/login-vs-register" title="デスノートを取り出す(ログイン)">
                  <i class="icon-signin"></i>
                  <?php _e('デスノートを取り出す(ログイン)','deathnote')?>
                  </a>
                </li>
              <?php endif?>
            </ul>
          </div>
        </nav>
        <div class="f_left language_selector"></div>
        <div class="clearfix"></div>
      </div>
      <!-- End col-md-* -->
      <div class="col-md-6">
        <div class="clearfix"></div>
      </div>
      <!-- End col-md-* -->
    </div>
    <!-- End row -->
  </section>
  <!-- End container -->
</div>
<!-- End header-top -->
<header class="header_light " id="header">
  <section class="container clearfix">
    <div class="logo">
      <a class="logo-img" href="/">
        <img alt="logo" class="default_logo" height="123" itemprop="logo" src="<?php bloginfo('template_url') ?>/assets/images/logo.png" width="405">
        <img alt="logo" class="retina_logo" height="123" itemprop="logo" src="<?php bloginfo('template_url') ?>/assets/images/logo.png" width="405">
      </a>
      <meta content="3" itemprop="name"></meta>
    </div>
    <nav class="navigation">
      <div class="header-menu">
        <ul class="">
          <li class="menu-item menu-item-type-post_type menu-item-object-page">
            <a class="" href="/add-deathnote"><?php _e('デスノートを書く','deathnote')?></a>
          </li>
        </ul>
      </div>
    </nav>
    <div class="mobile-menu"><div class="mobile-menu-click navigation_mobile"></div></div>
  </section>
  <!-- End container -->
</header>
<!-- End header -->