<footer class=" no-footer" id="footer-bottom">
  <section class="container">
    <div class="copyrights f_left">
      © 2015-2018 だんなDEATH NOTE
      <a href="index.html">
      https://danna-shine.com/
      </a>
      当サイトに書かれている内容を許可なく無断で転載、引用、紹介することを固く禁じます。
    </div>
  </section>
  <!-- End container -->
</footer>
<!-- End footer-bottom -->