<aside class="col-md-3 sidebar">
  <div class="widget widget_nav_menu" id="nav_menu-6">
    <h3 class="widget_title">
      Ranking By View
    </h3>
    <div>
      <?php do_action('show_list_popular_post_side_bar') ?>
    </div>
  </div>
  <div class="widget widget_text" id="text-17">
    <h3 class="widget_title">
      オフ会の報告と募集
    </h3>
    <div class="textwidget">
      <ul>
        <li>
          <a href="#">2018.04.23 第４回 女子会の報告</a>
        </li>
        <li>
          <a href="#">2017.10.21 第３回 オフ会の報告(テレビ)</a>
        </li>
        <li>
          <a href="#">女子会BMCの設立と募集</a>
        </li>
        <li>
          <a href="#">2017.6.26 第２回 オフ会の報告</a>
        </li>
        <li>
          <a href="#">2017.3.11 第１回 オフ会の報告</a>
        </li>
      </ul>
    </div>
  </div>
  <div class="widget questions_categories-widget" id="questions_categories-widget-2">
    <h3 class="widget_title">
      カテゴリ
    </h3>
    <ul>
      <li>
        <a href="#">だんなに死んでほしい<span>(<span>17936 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">だんなと離婚したい<span>(<span>1577 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">結婚直前の自分に教えたいこと<span>(<span>36 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">だんなに贈る川柳５・７・５<span>(<span>526 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">同じ状況の女性に聞いてみたい<span>(<span>322 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">密かな旦那への復讐<span>(<span>292 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">彼氏に死んで欲しい<span>(<span>257 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">だんなと再構築したい<span>(<span>155 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">旦那のせいで死にたい<span>(<span>87 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">わたしの汚料理レシピ<span>(<span>32 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">デスノート効果あり報告<span>(<span>52 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">旦那を許す<span>(<span>2 デス書き込み</span>)</span></a>
      </li>
      <li>
        <a href="#">離婚して自立した経験談<span>(<span>26 デス書き込み</span>)</span></a>
      </li>
    </ul>
  </div>
  <div class="widget_text widget widget_custom_html" id="custom_html-2">
    <h3 class="widget_title">
      よくある記事の検索
    </h3>
    <div class="textwidget custom-html-widget">
      <a href="#">モラハラ</a>
      <a href="#">DV</a>
      <a href="#">暴力</a>
      <a href="#">浮気</a>
      <a href="#">不倫</a>
      <a href="#">セックスレス</a>
      <a href="#">ドケチ</a>
      <a href="#">虐待</a>
      <a href="#">マザコン</a>
      <a href="#">借金</a>
      <a href="#">出会い系</a>
      <a href="#">ゲーム</a>
      <a href="#">パチンコ</a>
      <a href="#">アスペ(アスペルガー症候群)</a>
      <a href="#">カサンドラ</a>
      <a href="#">発達障害</a>
      <a href="#">ヒモ</a>
      <a href="#">アルコール依存</a>
      <a href="#">ギャンブル依存</a>
      <a href="#">スマホ依存</a>
    </div>
  </div>
  <div class="widget widget_nav_menu" id="nav_menu-3">
    <h3 class="widget_title">死亡報告</h3>
    <div>
      <ul>
        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-26993" id="menu-item-26993">
          <a class="" href="#">未亡人になりました　2018.04.07</a>
        </li>
        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-22882" id="menu-item-22882">
          <a class="" href="#">命は戻らない　2018.03.07</a>
        </li>
        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-22885" id="menu-item-22885">
          <a class="" href="#">死にました　2018.03.06</a>
        </li>
        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-22884" id="menu-item-22884">
          <a class="" href="#">死にました　2018.03.04</a>
        </li>
        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-22883" id="menu-item-22883">
          <a class="" href="#">本当に死にました　2018.02.25</a>
        </li>
        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4617" id="menu-item-4617">
          <a class="" href="#">永眠　2016.04.28</a>
        </li>
        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4616" id="menu-item-4616">
          <a class="" href="#">即死　2016.04.17</a>
        </li>
      </ul>
    </div>
  </div>
  <div class="widget widget_nav_menu" id="nav_menu-4">
    <h3 class="widget_title">
      Information
    </h3>
    <div class="menu-information-container">
      <ul class="menu" id="menu-information">
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2492" id="menu-item-2492">
          <a class="" href="#" title="旦那DEATH NOTEとは">だんなデスノートとは</a>
        </li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6134" id="menu-item-6134">
          <a class="" href="#" title="旦那DEATH NOTEヘルプ">だんなデスノートヘルプ</a>
        </li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6205" id="menu-item-6205">
          <a class="" href="#" title="死神の声(ブログ)">死神の声(ブログ)</a>
        </li>
        <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-6392" id="menu-item-6392">
          <a class="" href="#" title="メディア掲載実績">メディア掲載実績</a>
        </li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2493" id="menu-item-2493">
          <a class="" href="#" title="問い合わせ">死神に問い合わせ</a>
        </li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3970" id="menu-item-3970">
          <a class="" href="#" title="利用規約">利用規約</a>
        </li>
        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6669" id="menu-item-6669">
          <a class="" href="#">ツイッター@danna_shine_com</a>
        </li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18380" id="menu-item-18380">
          <a class="" href="#">運営者について</a>
        </li>
      </ul>
    </div>
  </div>
  <div class="widget widget_text" id="text-16">
    <h3 class="widget_title">
      だんなデスノート姉妹版
    </h3>
    <div class="textwidget">
      <ul>
        <li>
          <a href="#">愛人デスノート</a><br/>愛人に今すぐ死んで欲しい。毎日思っている。不倫しておいて普通に生活できると思うなよ。ほんと最低のクズだわ。マジでキモイ
        </li>
        <li>
          <a href="#">毒親デスノート</a><br/>母親、父親に今すぐ死んでほしい。毎日思っている。あなたの子供になんか産まれたくなかった。
        </li>
        <li>
          <a href="#">義父母デスノート</a><br/>姑がうざい! 旦那じゃなくて義母、義父に言いたいことがある!
        </li>
        <li>
          <a href="#">上司デスノート</a><br/>上司がうざい! 上司に言えないことを言ってみるSNS
        </li>
        <li>
          <a href="#">お客様デスノート</a><br/>客は神様じゃねーんだよ! 偉そうな客に正義の鉄槌を
        </li>
        <li>
          <a href="#">ちょっとだけ、バイバイ</a><br/>もう逢えないあの人に思いを届けてあげる魔法のサイト
        </li>
        <li>
          <a href="#">旦那スキですノート</a><br/>たまにはいいよね？奥さんから旦那さんへの愛の気持ち語ってみる
        </li>
        <li>
          <a href="#">嫁スキですノート</a><br/>普段は言えない! 旦那さんから嫁さんへの愛の気持ちを語ってみた
        </li>
      </ul>
    </div>
  </div>
  <div class="widget widget_text" id="text-18">
    <h3 class="widget_title">
      PR
    </h3>
    <div class="textwidget">
      <p>
        <a href="http://clk-fm-report.net/?id=337" rel="nofollow" title="ぴゅあじょ">
        <img src="<?php bloginfo('template_url')?>/assets/images/purelovers_2017-12-07_500x500.jpg"/>
        </a>
      </p>
      <p>
        <a href="https://www.tantei-mr.co.jp/lp/uwaki/61.html?t=1" rel="nofollow" title="総合探偵社MR">
        <img src="<?php bloginfo('template_url')?>/assets/images/2018-02-06_500x500.jpg"/>
        </a>
      </p>
    </div>
  </div>
</aside>
<!-- End sidebar -->