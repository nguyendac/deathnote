<div class="panel-pop" id="signup">
  <h2>
    デスノートを拾う(無料登録)
    <i class="icon-remove">
    </i>
  </h2>
  <div class="form-style form-style-3">
    <form class="signup_form ask_form" enctype="multipart/form-data" method="post">
      <div class="ask_error">
      </div>
      <div class="form-inputs clearfix">
        <p>
          <label class="required" for="user_name_707">契約者ID( 英数字のみ )<span>*</span></label>
          <input class="required-item" id="user_name_707" name="user_name" type="text" value=""></input>
        </p>
        <p>
          <label class="required" for="email_707">メールアドレス<span>*</span></label>
          <input class="required-item" id="email_707" name="email" type="email" value=""></input>
        </p>
        <p>
          <label class="required" for="pass1_707">パスワード<span>*</span></label>
          <input autocomplete="off" class="required-item" id="pass1_707" name="pass1" type="password"></input>
        </p>
        <p>
          <label class="required" for="pass2_707">パスワードの確認<span>*</span></label>
          <input autocomplete="off" class="required-item" id="pass2_707" name="pass2" type="password"></input>
        </p>
        <p class="ask_captcha_p">
          <label class="required" for="ask_captcha_707">画像の文字を入力<span>*</span></label>
          <input class="ask_captcha" id="ask_captcha_707" name="ask_captcha" size="10" type="text" value="">
          <img alt="画像の文字を入力" class="ask_captcha_img" id="ask_captcha_img_707" onclick="javascript:ask_get_captcha('https://danna-shine.com/cms/wp-content/themes/danna-shine_53of/captcha/create_image.php', 'ask_captcha_img_707');" src="https://danna-shine.com/cms/wp-content/themes/danna-shine_53of/captcha/create_image.php" title="文字が見づらい場合は画像をクリックしてください">
          <span class="question_poll ask_captcha_span">クリックすると違う画像が表示されます</span></img>
          </input>
        </p>
      </div>
      <p class="form-submit">
        <input name="redirect_to" type="hidden" value="">
        <input class="button color small submit" name="register" type="submit" value="覚悟を持ってデスノートを拾う( 登録 )">
        <input name="form_type" type="hidden" value="ask-signup">
        </input>
        </input>
        </input>
      </p>
    </form>
  </div>
</div>
<!-- End signup -->
<div class="panel-pop" id="login-comments">
  <h2>
    デスノートを取り出す(ログイン)
    <i class="icon-remove">
    </i>
  </h2>
  <div class="form-style form-style-3">
    <div class="ask_form inputs">
      <form action="" class="login-form ask_login" method="post">
        <div class="ask_error">
        </div>
        <div class="form-inputs clearfix">
          <p class="login-text">
            <input class="required-item" name="log" onblur="if (this.value == '') {this.value = '契約者ID( 英数字のみ )';}" onfocus="if (this.value == '契約者ID( 英数字のみ )') {this.value = '';}" type="text" value="契約者ID( 英数字のみ )">
            <i class="icon-user">
            </i>
            </input>
          </p>
          <p class="login-password">
            <input class="required-item" name="pwd" onblur="if (this.value == '') {this.value = 'パスワード';}" onfocus="if (this.value == 'パスワード') {this.value = '';}" type="password" value="パスワード">
            <i class="icon-lock">
            </i>
            <a href="#">
            パスワードを忘れた
            </a>
            </input>
          </p>
        </div>
        <p class="form-submit login-submit">
          <span class="loader_2">
          </span>
          <input class="button color small login-submit submit sidebar_submit" type="submit" value="デスノートを取り出す(ログイン)">
          </input>
        </p>
        <div class="rememberme">
          <label>
          <input checked="checked" input="" name="rememberme" type="checkbox">
          記憶する
          </input>
          </label>
        </div>
        <input name="redirect_to" type="hidden" value="">
        <input name="login_nonce" type="hidden" value="ef2b80ca61">
        <input name="ajax_url" type="hidden" value="">
        <input name="form_type" type="hidden" value="ask-login">
        <div class="errorlogin">
        </div>
        </input>
        </input>
        </input>
        </input>
      </form>
    </div>
  </div>
</div>
<!-- End login-comments -->
<div class="panel-pop" id="lost-password">
  <h2>パスワードを忘れてしまった<i class="icon-remove"></i></h2>
  <div class="form-style form-style-3">
    <p>
      パスワードを忘れてしまった場合は、登録時に使用されたメールアドレスを下記に入力し、「リセットする」をクリックしてください。パスワード再設定用のメールが届きます。
    </p>
    <form action="" class="ask-lost-password ask_form" method="post">
      <div class="ask_error">
      </div>
      <div class="form-inputs clearfix">
        <p>
          <label class="required" for="user_mail_488">
          メールアドレス
          <span>
          *
          </span>
          </label>
          <input class="required-item" id="user_mail_488" name="user_mail" type="email">
          </input>
        </p>
      </div>
      <p class="form-submit">
        <input class="button color small submit" type="submit" value="リセット">
        <input name="form_type" type="hidden" value="ask-forget">
        </input>
        </input>
      </p>
    </form>
    <div class="clearfix">
    </div>
  </div>
</div>
<!-- End lost-password -->