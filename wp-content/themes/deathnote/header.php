<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <link rel='stylesheet' id='open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext&#038;ver=4.8.6' type='text/css' media='all' />
  <link href="<?php bloginfo('template_url')?>/assets/css/base.css" id="v_base-css" media="all" rel="stylesheet" type="text/css"/>
  <!-- <link href="https://danna-shine.com/cms/wp-content/themes/danna-shine_53of/css/lists.css?ver=4.8.6" id="v_lists-css" media="all" rel="stylesheet" type="text/css"/> -->
  <link href="<?php bloginfo('template_url')?>/assets/css/bootstrap.min.css" id="v_bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
  <link href='<?php bloginfo('template_url')?>/assets/css/font/font-awesome-old/font-awesome.min.css' rel='stylesheet' id='v_font_awesome_old-css'   type='text/css' media='all' />
  <link href="<?php bloginfo('template_url')?>/assets/css/font/font-awesome/font-awesome.min.css" id="v_font_awesome-css" media="all" rel="stylesheet" type="text/css"/>
  <link href="<?php bloginfo('template_url')?>/assets/css/font/fontello/fontello.css" id="v_fontello-css" media="all" rel="stylesheet" type="text/css"/>
  <link href="<?php bloginfo('template_url')?>/assets/css/style.css" id="v_css-css" media="all" rel="stylesheet" type="text/css"/>
  <link href="<?php bloginfo('template_url')?>/assets/css/responsive.css" id="v_responsive-css" media="all" rel="stylesheet" type="text/css"/>
  <link href="<?php bloginfo('template_url')?>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
  <link rel='dns-prefetch' href='//fonts.googleapis.com' />
  <?php wp_head();?>
</head>
