<?php
/*
  Template Name: Add Deathnote Page
 */
 if( !session_id()){
   session_start();
 }
  if(!get_current_user_id()){
    wp_safe_redirect(get_bloginfo('url').'/login-vs-register');
    exit;
  }

  if(isset($_POST['addnote'])) {
   $result = apply_filters('result_addnote',$_POST);
  }
  if(flash('add_deathnote')) {
    echo flash('add_deathnote');
  }
  if(flash('error')) {
    echo flash('error');
  }
  get_header();
?>
<body>
  <div class="background-cover"></div>
  <?php get_template_part('template/template','popup')?>
  <?php get_template_part('template/template','mobile')?>
  <div id="wrap" class="grid_1200 boxed">
    <?php get_template_part('template/template','header')?>
    <div class="breadcrumbs">
      <section class="container">
        <div class="row">
          <div class="col-md-12">
            <h1>デスノートを拾う(ログインor登録)</h1>
            <div class="clearfix"></div>
            <div class="crumbs">
              <a itemprop="breadcrumb" href="/">トップページ</a><span class="crumbs-span">/</span> <span class="current">デスノートを拾う(ログインor登録)</span>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class='index-no-box'></div>
    <?php get_template_part('template/template','search')?>
    <div class="clearfix"></div>
    <section class="container main-content page-full-width">
      <div class="row">
        <div class="with-sidebar-container">
          <div class="main-sidebar-container col-md-12">
            <div class="page-content">
              <div class="boxedtitle page-title">
                <h2>デスノートを書く</h2>
              </div>
              <div class="form-posts">
                <div class="form-style form-style-3 question-submit">
                  <div class="ask_question">
                    <div>
                      <form class="new-question-form" method="post" enctype="multipart/form-data" action="">
                        <div class="note_error display"></div>
                        <div class="form-inputs clearfix">
                          <div class="details-area">
                            <label for="question-details-1000" class="required">タイトル<span>*</span></label>
                            <input type="text" name="title" id="question-details-1000" class="the-text" aria-required="true"/>
                            <div class="clearfix"></div>
                          </div>
                          <div class="div_category">
                            <label for="question-category-969" class="required">カテゴリ<span>*</span></label>
                            <span class="styled-select">
                              <select name="category" id="question-category-969" class="postform">
                                <option class="level-0" value="">Please choose one ...</option>
                                <?php
                                  $types = get_taxo('type');
                                  foreach($types as $type) :
                                ?>
                                <option class="level-0" value="<?php _e($type->term_id) ?>"><?php _e(nl2br($type->name)) ?></option>
                              <?php endforeach;?>
                              </select>
                            </span>
                            <span class="form-description">カテゴリを選択してください</span>
                          </div>
                          <label for="featured_image-969">死因・日数・似顔絵の写メ</label>
                          <div class="fileinputs">
                            <input type="file" class="file" name="featured_image" id="featured_image-969">
                            <div class="fakefile">
                              <button type="button" class="button small margin_0">ファイルを選択</button>
                              <span><i class="icon-arrow-up"></i>参照する</span>
                            </div>
                          </div>
                        </div>
                        <div class="details-area">
                          <label for="question-details-969" class="required">メッセージ<span>*</span></label>
                          <textarea name="comment" id="question-details-969" class="the-textarea" aria-required="true" cols="58" rows="8"></textarea>
                          <div class="clearfix"></div>
                        </div>
                        <div class="form-inputs clearfix">
                          <p class="question_poll_p">
                            <label for="remember_answer-969">通知</label>
                            <input type="checkbox" id="remember_answer-969" name="remember_answer" value="1" checked="checked">
                            <span class="question_poll">コメントがあったらメールで通知する</span>
                          </p>
                          <!-- <p class="ask_captcha_p">
                            <label for="ask_captcha_969" class="required">画像の文字を入力<span>*</span></label>
                            <input size="10" id="ask_captcha_969" name="ask_captcha" class="ask_captcha" value="" type="text"><img class="ask_captcha_img" src="https://danna-shine.com/cms/wp-content/themes/danna-shine_53of/captcha/create_image.php" alt="画像の文字を入力" title="文字が見づらい場合は画像をクリックしてください" onclick="javascript:ask_get_captcha('https://danna-shine.com/cms/wp-content/themes/danna-shine_53of/captcha/create_image.php', 'ask_captcha_img_969');" id="ask_captcha_img_969">
                            <span class="question_poll ask_captcha_span">クリックすると違う画像が表示されます</span>
                          </p> -->
                        </div>
                        <p class="form-submit">
                          <input type="hidden" name="post_type" value="add_question">
                          <input type="hidden" name="form_type" value="add_question">
                          <input type="submit" value="だんなデスノートへ書き込む" class="button color small submit add_qu publish-question" name="addnote">
                        </p>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- End page-content -->
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- End with-sidebar-container -->
      </div>
      <!-- End row -->
    </section>
    <?php get_template_part('template/template','footer')?>
  </div>
<?php get_footer();?>