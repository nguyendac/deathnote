<?php
require_once("meta-box-class/my-meta-box-class.php");
require_once("Tax-meta-class/tax-meta-class.php");
require_once("helper/get_list.php");
require_once("helper/ajax.php");
require_once("helper/validation.php");
function wpse66093_no_admin_access() {
    $redirect = isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : home_url( '/' );
    if ( current_user_can('editor'))
      exit( wp_redirect( $redirect ) );
}
add_action( 'admin_init', 'wpse66093_no_admin_access', 100 );
function flash( $name = '', $message = '',$class = 'success fadeout-message') {
  //We can only do something if the name isn't empty
  if(!empty($name)) {
    //No message, create it
    if(!empty( $message ) && empty( $_SESSION[$name])){
      if( !empty( $_SESSION[$name] ) ){
        unset( $_SESSION[$name] );
      }
      if( !empty( $_SESSION[$name.'_class'] ) ){
        unset( $_SESSION[$name.'_class'] );
      }
      $_SESSION[$name] = $message;
      $_SESSION[$name.'_class'] = $class;
    }
    //Message exists, display it
    elseif( !empty( $_SESSION[$name] ) && empty( $message ) ){
      echo $_SESSION[$name];
      unset($_SESSION[$name]);
      unset($_SESSION[$name.'_class']);
    }
  }
}
function wpse_139269_term_radio_checklist($args) {
  if (!empty($args['taxonomy']) && (($args['taxonomy'] === 'type'))) {
    if (empty($args['walker']) || is_a($args['walker'], 'Walker')) { // Don't override 3rd party walkers.
      if (!class_exists('WPSE_139269_Walker_Category_Radio_Checklist')) {
        /**
         * Custom walker for switching checkbox inputs to radio.
         *
         * @see Walker_Category_Checklist
         */
        class WPSE_139269_Walker_Category_Radio_Checklist extends Walker_Category_Checklist {
          function walk($elements, $max_depth, $args = array()) {
            $output = parent::walk($elements, $max_depth, $args);
            $output = str_replace(
                    array('type="checkbox"', "type='checkbox'"), array('type="radio"', "type='radio'"), $output
            );
            return $output;
          }
        }
      }
      $args['walker'] = new WPSE_139269_Walker_Category_Radio_Checklist;
    }
  }
  return $args;
}
// add_filter('wp_terms_checklist_args', 'wpse_139269_term_radio_checklist');
// remove admin bar
add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
  remove_action('wp_head', '_admin_bar_bump_cb');
}
// deathnote
function deathnote_arr(){
  $args = array(
    'public'=>true,
    'labels' => array(
      'name'=> 'List Deathnote',
      'singular_name'=> 'deathnote',
      'add_new_item'=> 'New Deathnote'
    ),
    'supports' => array(
      'title',
      'comments'
    ),
  );
  register_post_type('deathnote', $args);
}
add_action('init', 'deathnote_arr');
$config_deathnote = array(
  'id' => 'deathnote',
  'title' => 'Deathnote',
  'pages' => array('deathnote'),
  'context' => 'normal',
  'priority' => 'high',
  'fields' => array(),
  'local_images' => true,
  'use_with_theme' => true
);
$deathnote_meta = new AT_Meta_Box($config_deathnote);
$deathnote_meta->addImage('image_field',array('name'=>'Image Deathnote','style'=>'max-width :100%; width:100%; height:auto'));
$deathnote_meta->addTextarea('content_deathnote',array('name'=>'Content Deathnote','style'=>'max-width :100%; width:100%'));
$deathnote_meta->Finish();
// taxonomy deathnote
function create_type_deathnote(){
  $labels = array(
    'name'              => _x( 'Name By Type Deathnote', 'taxonomy general name' ),
    'singular_name'     => _x( 'By Type Deathnote', 'taxonomy singular name' ),
    'search_items'      => __( 'Search By Type Deathnote' ),
    'all_items'         => __( 'All By Type Deathnote' ),
    'parent_item'       => __( 'Parent By Type Deathnote' ),
    'parent_item_colon' => __( 'Parent By Type Deathnote:' ),
    'edit_item'         => __( 'Edit By Type Deathnote' ),
    'update_item'       => __( 'Update By Type Deathnote' ),
    'add_new_item'      => __( 'Add New By Type Deathnote' ),
    'new_item_name'     => __( 'New By Type Deathnote' ),
    'menu_name'         => __( 'By Type Deathnote' ),
  );
  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'type-deathnote' ),
  );
  register_taxonomy( 'type', array( 'deathnote' ), $args );
}
add_action( 'init', 'create_type_deathnote', 0 );
function get_taxo($taxo){
  $terms = get_terms(array(
    'taxonomy' => $taxo,
    'hide_empty' => false,
    'orderby' => 'ID',
    'order' => 'asc',
    'parent' => 0
  ));
  return $terms;
}
// count view
function postview_set($postID) {
  $count_key = 'postview_number';
  $count = get_post_meta($postID, $count_key, true);
  if ($count == '') {
    $count = 0;
    delete_post_meta($postID, $count_key);
    add_post_meta($postID, $count_key, '0');
  } else {
    $count++;
    update_post_meta($postID, $count_key, $count);
  }
}

function postview_get($postID) {
  $count_key = 'postview_number';
  $count = get_post_meta($postID, $count_key, true);
  if ($count == '') {
    delete_post_meta($postID, $count_key);
    add_post_meta($postID, $count_key, '0');
      return "0";
  }
  return $count;
}
// register
// count like
function action_register($post) {
  global $wpdb;
  $username = esc_attr($post['user_name']);
  $email = esc_attr($post['email']);
  $pw1 = esc_attr($post['pass1']);
  $pw2 = esc_attr($post['pass2']);
  $checkuser = [];
  if(!$username || !$email || !$pw1 || !$pw2) {
    wp_safe_redirect(get_bloginfo('url').'/login-vs-register');
    flash('required','上記の項目は必須です');
    exit;
  }
  if($username && $email) {
    $checkuser = $wpdb->get_results("SELECT ID FROM `".$wpdb->prefix."users` WHERE `user_login` = '".$username."' OR `user_email` = '".$email."'");
    if(count($checkuser)) {
      wp_safe_redirect(get_bloginfo('url').'/login-vs-register');
      flash('unique','username or email exist');
      exit;
    } else {
      if($pw1 != $pw2) {
        wp_safe_redirect(get_bloginfo('url').'/login-vs-register');
        flash('password','password not correct');
        exit;
      } else {
        $userdata = array(
          'user_login'=> $username,
          'user_email' => $email,
          'user_pass'  => $pw1
        );
        $user_id = wp_insert_user( $userdata );
        $user = new WP_User($user_id);
        $user->add_role('editor');
        wp_safe_redirect(get_bloginfo('url').'/login-vs-register');
        flash('success','Register Success');
        exit;
      }
    }
  } else {
    if($username) {
      $checkuser = $wpdb->get_results("SELECT ID FROM `".$wpdb->prefix."users` WHERE `user_login` = '".$username."'");
    }
    if($email) {
      $checkuser = $wpdb->get_results("SELECT ID FROM `".$wpdb->prefix."users` WHERE `user_email` = '".$email."'");
    }
  }
}
add_filter('result_register','action_register');
// end register
function action_mypage($post){
  global $wpdb;
  $username = esc_attr($post['user_name']);
  $email = esc_attr($post['email']);
  $pw1 = esc_attr($post['pass1']);
  $pw2 = esc_attr($post['pass2']);
  $checkuser = [];
  if(!$username || !$email) {
    wp_safe_redirect(get_bloginfo('url').'/my-page');
    flash('notice','上記の項目は必須です');
    exit;
  }
  if($username && $email) {
    $checkuser = $wpdb->get_results("SELECT ID FROM `".$wpdb->prefix."users` WHERE (`user_nicename` = '".$username."' OR `user_email` = '".$email.")' AND ID NOT IN(2)");
    if(count($checkuser)) {
      wp_safe_redirect(get_bloginfo('url').'/my-page');
      flash('notice','username or email exist');
      exit;
    } else {
      if($_FILES['you_avatar']) {
        $a = upload_avatar('you_avatar',get_current_user_id());
      }
      if($pw1) {
        if($pw1 != $pw2) {
          wp_safe_redirect(get_bloginfo('url').'/my-page');
          flash('notice','password not correct');
          exit;
        } else {
          $userdata = array(
            'ID' => get_current_user_id(),
            'user_nicename'=> $username,
            'user_email' => $email,
            'user_pass'  => $pw1
          );
        }
      } else {
        $userdata = array(
          'ID' => get_current_user_id(),
          'user_nicename'=> $username,
          'user_email' => $email,
        );
      }
      $user_id = wp_update_user( $userdata );
      // $user = new WP_User($user_id);

      wp_safe_redirect(get_bloginfo('url').'/my-page');
      flash('notice','Update Success');
      exit;
    }
  }
}
add_filter('result_mypage','action_mypage');
// end mypage
// login
function action_login($post){
  global $wpdb;
  $username = $post['log'];
  $password = $post['pwd'];
  if( $username == "" || $password == "" ) {
      $err = 'Please don\'t leave the required field.';
    } else {
      $user_data = array();
      $user_data['user_login'] = $username;
      $user_data['user_password'] = $password;
      $user = wp_signon( $user_data, false );

      if ( is_wp_error($user) ) {
        $err = $user->get_error_message();
        wp_safe_redirect(get_bloginfo('url').'/login-vs-register');
        flash('login_fail',$err);
        exit;
      } else {
        wp_set_current_user( $user->ID, $username );
        do_action('set_current_user');

        wp_safe_redirect(get_bloginfo('url').'/add-deathnote');
        flash('login_success','Login Success');
        exit;
      }
    }
}
add_filter('result_login','action_login');
// add deathnote
function action_deathnote($post) {
  $cate = $post['category'];
  $comment = $post['comment'];
  $title = $post['title'];
  if(!$cate || !$comment || !$title) {
    wp_safe_redirect(get_bloginfo('url').'/add-deathnote');
    flash('add_deathnote','Add Deathnote Fail');
    exit;
  }
  if($cate && $comment && $title) {
    if ($_FILES['featured_image']['size'] != 0 && $_FILES['featured_image']['error'] == 0) {
      if(!check_img($_FILES['featured_image'])) {
        wp_safe_redirect(get_bloginfo('url').'/add-deathnote');
        flash('error','Image Error');
        exit;
      }
    }
    $post_id = wp_insert_post(array(
      'post_type'=> 'deathnote',
    ));
    $update = array(
      'ID' => $post_id,
      'post_status'=>'publish',
      'post_title' => $title
    );
    wp_update_post($update);
    if($post_id) {
      $term = get_term($cate,'type');
      wp_set_object_terms($post_id,$term->name,'type');
      add_post_meta($post_id,'content_deathnote',$comment);
      comments_open($post_id);
    }
    if ($_FILES['featured_image']['size'] != 0 && $_FILES['featured_image']['error'] == 0) {
      $a = upload_image('featured_image',$post_id,'no-caption');
    }
  }
}
add_filter('result_addnote','action_deathnote');
function check_img($file) {
  if(!check_file_type($file) || human_size($file['size']) > 500 ) {
    return false;
  }
  return true;
}
function check_file_type($file){
  $allowedTypes = array('image/jpeg','image/png','image/gif','image/jpg');
  $detectedType = $file['type'];
  $error = in_array($detectedType, $allowedTypes);
  return $error;
}
function human_size($bytes) {
  $bytes = floatval($bytes);
    $arBytes = array(
      0 => array(
          "UNIT" => "KB",
          "VALUE" => 1024
      )
    );
    foreach($arBytes as $arItem) {
      $result = $bytes / $arItem["VALUE"];
      $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
      break;
    }
    return floatval($result);
}
function upload_image($files,$post_id,$caption) {
  require_once(ABSPATH . "wp-admin" . '/includes/image.php');
  require_once(ABSPATH . "wp-admin" . '/includes/file.php');
  require_once(ABSPATH . "wp-admin" . '/includes/media.php');
  $attachment_id = media_handle_upload($files, $post_id);
  $attachment_url = wp_get_attachment_url($attachment_id);
  $image = array(
    'id'=> $attachment_id,
    'url' => $attachment_url
  );
  $image_field = serialize($image);
  add_post_meta($post_id, 'image_field', $image);
  return $attachment_id;
}
function upload_avatar($files,$user_id) {
  require_once(ABSPATH . "wp-admin" . '/includes/image.php');
  require_once(ABSPATH . "wp-admin" . '/includes/file.php');
  require_once(ABSPATH . "wp-admin" . '/includes/media.php');
  $attachment_id = media_handle_upload($files, $user_id);
  $attachment_url = wp_get_attachment_url($attachment_id);
  $image = array(
    'id'=> $attachment_id,
    'url' => $attachment_url
  );
  $image_field = serialize($image);
  update_user_meta($user_id, 'avatar', $image);
  return $attachment_id;
}
add_filter( 'comments_open', 'my_comments_open', 10, 2 );
function my_comments_open( $open, $post_id ) {
  $post = get_post( $post_id );
  if ( 'deathnote' == $post->post_type )
    $open = true;
  return $open;
}
// pagination
function mp_pagination($prev = 'Prev', $next = 'Next', $pages='') {
    global $wp_query, $wp_rewrite, $textdomain;
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
    if($pages==''){
        global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
    }
    if(array_key_exists('pg',$_GET)){
      if($_GET['pg']) {
        $pg = $_GET['pg'];
      } else {
        $pg = 1;
      }
    } else {
      $pg = 1;
    }
    $big = 999999999;
    $pagination = array(
    'base'      => @add_query_arg( 'pg', '%#%' ),
    'format'   => '',
    'current'   => max ( 1, $pg),
    'total'   => $pages,
    'prev_text' => __($prev,$textdomain),
    'next_text' => __($next,$textdomain),
    'type'   => 'plain',
    'end_size'  => 2,
    'mid_size'  => 2
    );
    $return =  paginate_links( $pagination );
    // echo str_replace( "<ul class='page-numbers'>", '<ul class="pagination">', $return );
    echo $return;
}
function my_enqueue() {
  wp_enqueue_script('jquery');
  wp_enqueue_script( 'comment-reply' );
  wp_enqueue_script('handling_ajax', get_template_directory_uri().'/assets/js/script.js', '1.0', 1 );
  wp_localize_script('handling_ajax', 'admin_url', admin_url('admin-ajax.php'));
}
add_action( 'wp_enqueue_scripts', 'my_enqueue');
function getlike($id) {
  $list = get_post_meta($id,'users_like',true);
  // string --> array
  $array = explode(',',$list);
  return count($array) - 1;
}
function getlikecomment($comment) {
  $list = get_comment_meta($comment,'users_like',true);
  // string --> array
  $array = explode(',',$list);
  return count($array) - 1;
}
function my_remove_email_field_from_comment_form($fields) {
  if(isset($fields['email'])) unset($fields['email']);
  return $fields;
}
add_filter('comment_form_default_fields', 'my_remove_email_field_from_comment_form');
function my_remove_url_field_from_comment_form($fields) {
  if(isset($fields['url'])) unset($fields['url']);
  return $fields;
}
add_filter('comment_form_default_fields', 'my_remove_url_field_from_comment_form');
function wpb_move_comment_field_to_bottom( $fields ) {
$comment_field = $fields['comment'];
unset( $fields['comment'] );
$fields['comment'] = $comment_field;
return $fields;
}
add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );
add_filter( 'cancel_comment_reply_link', '__return_false' );

function show_popular_post_side_bar() {
  $now = new DateTime();
  $year = $now->format("Y");
  $week = $now->format("W");
  $date_range = strtotime ( '-7 day' );
  $args = array(
    'post_type' => 'deathnote',
    'date_query' => array(
        array(
            'after' => '1 week ago'
        ),
    ),
    'meta_key' => 'postview_number',
    'orderby' => 'meta_value_num',
    'posts_per_page' => 10,
  );?>
  <?php $query = new WP_Query($args);
  if ($query->have_posts()):
    while ($query->have_posts()): $query->the_post();?>
      <div class="recent-post-box clearfix wow fadeInDown news_post_sidebar">
        <a href="<?php the_permalink();?>"><?php the_title();?>
          <br>
          <span class="view_count"><?php echo postview_get(get_the_ID()) ?></span>
        </a>
      </div>
      <?php endwhile;?>
  <?php endif;}?>
<?php add_action('show_list_popular_post_side_bar', 'show_popular_post_side_bar', 1);

function show_popular_post_like_side_bar() {
  $now = new DateTime();
  $args = array(
    'post_type' => 'deathnote',
    'date_query' => array(
        array(
            'after' => '1 week ago'
        ),
    ),
    'meta_key' => 'total_like',
    'orderby' => 'meta_value_num',
    'posts_per_page' => 10,
  );?>
  <?php $query = new WP_Query($args);
  if ($query->have_posts()):
    while ($query->have_posts()): $query->the_post();?>
      <div class="recent-post-box clearfix wow fadeInDown news_post_sidebar">
        <a href="<?php the_permalink();?>"><?php the_title();?>
          <br>
          <span class="view_count"><?php echo getlike(get_the_ID()) ?></span>
        </a>
      </div>
      <?php endwhile;?>
  <?php endif;}?>
<?php add_action('show_list_popular_post_like_side_bar', 'show_popular_post_like_side_bar', 1);
add_filter( "radio_buttons_for_taxonomies_no_term_type", "__return_FALSE" );
// add script for admin
add_action('admin_enqueue_scripts', 'wdscript');
function wdscript() {
  wp_register_style('custom_wp_admin_css', get_template_directory_uri() . '/admin-style.css', false, '1.0.0');
  wp_enqueue_style('custom_wp_admin_css');
}