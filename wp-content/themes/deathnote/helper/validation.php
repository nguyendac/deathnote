<?php
add_action('admin_head-post.php','ep_publish_admin_hook');
add_action('admin_head-post-new.php','ep_publish_admin_hook');

function ep_publish_admin_hook(){
    global $post;
    if ( is_admin()){
        ?>
        <script language="javascript" type="text/javascript">
            (function($){
                jQuery(document).ready(function() {
                    jQuery('#post').submit(function(e) {
                      var $radio = $('input:radio[name="radio_tax_input[type][]"]:checked').length;
                      var $post_type = $('#post_type').val();
                      if($radio == 0 && $post_type == 'deathnote') {
                        e.preventDefault();
                        alert('please choose one')
                      }
                    });
                });
            })(jQuery);
        </script>
        <?php
    }
}
?>