<?php
function hanlinglike() {
  $msg = ('');
  if(!get_current_user_id()){
    $msg .= false;
  } else {
    $postId = $_POST['deathnote_id'];
    $user = 'users_like';
    $total_like = 'total_like';
    $list = get_post_meta($postId,$user,true);
    // string --> array
    $array = explode(',',$list);
    // check exists
    $key = array_search(get_current_user_id(),$array);
    if($key) {
      // unset($array[$key]);
      // // array -> string
      // $string = implode(',',$array);
      // update_post_meta($postId,$user,$string);
      $msg .= 2;
    } else {
      array_push($array,get_current_user_id());
      $total = count($array) - 1;
      // array -> string
      $string = implode(',',$array);
      update_post_meta($postId,$user,$string);
      update_post_meta($postId,$total_like,$total);
      $msg .= 1;
    }
  }
  die($msg);
}
add_action('wp_ajax_nopriv_hanlinglike', 'hanlinglike');
add_action('wp_ajax_hanlinglike', 'hanlinglike');

// like comment
function hanlinglikecomment() {
  $msg = ('');
  if(!get_current_user_id()){
    $msg .= false;
  } else {
    $postId = $_POST['comment_id'];
    $user = 'users_like';
    $list = get_comment_meta($postId,$user,true);
    // string --> array
    $array = explode(',',$list);
    // check exists
    $key = array_search(get_current_user_id(),$array);
    if($key) {
      // unset($array[$key]);
      // // array -> string
      // $string = implode(',',$array);
      // update_post_meta($postId,$user,$string);
      $msg .= 2;
    } else {
      array_push($array,get_current_user_id());
      // array -> string
      $string = implode(',',$array);
      update_comment_meta($postId,$user,$string);
      $msg .= 1;
    }
  }
  die($msg);
}
add_action('wp_ajax_nopriv_hanlinglikecomment', 'hanlinglikecomment');
add_action('wp_ajax_hanlinglikecomment', 'hanlinglikecomment');

// ask like search
function title_filter( $where, &$wp_query ) {
  global $wpdb;
  if ( $search_term = $wp_query->get( 'search_prod_title' ) ) {
      $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( like_escape( $search_term ) ) . '%\'';
  }
  return $where;
}
function ask_live_search(){
  $key = $_POST['search_value'];
  $msg = ('');
  $args = array(
    'post_type' => 'deathnote',
    'posts_per_page' => 8,
    'search_prod_title' => $key,
    'post_status' => 'publish',
    'orderby'     => 'id',
    'order'       => 'ASC'
  );
  add_filter( 'posts_where', 'title_filter', 10, 2 );
  $wp_query = new WP_Query($args);
  remove_filter( 'posts_where', 'title_filter', 10, 2 );
  $i = 0;
  if ($wp_query->have_posts()) {
    $msg .= '<div class="result-div"><ul>';
      while ($wp_query->have_posts()) : $wp_query->the_post();
        if($i < 6) {
          $link  = get_permalink(get_the_ID());
          $title = get_the_title(get_the_ID());
          $msg .= '<li>';
          $msg .= '<a href="'.$link.'">'.$title;
          $msg .= '</a>';
          $msg .= '</li>';
          $i++;
        }
        if($i >= 6) {
          $msg .= '<li>';
          $msg .= '<a href="/search-title?key='.$key.'">View all results.';
          $msg .= '</a>';
          $msg .= '</li>';
          break;
        }
      endwhile;
      wp_reset_postdata();
    $msg .= '</ul></div>';
  } else {
      $msg .= false;
  }
  die($msg);
}
add_action('wp_ajax_nopriv_ask_live_search', 'ask_live_search');
add_action('wp_ajax_ask_live_search', 'ask_live_search');
?>