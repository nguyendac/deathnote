<?php
function action_list($get) {
  global $wpdb;
  if(!$get) {
    $paged = 1;
  } else {
    if(array_key_exists('pg',$get)){
      if($get['pg']) {
        $paged = $get['pg'];
      } else {
        $paged = 1;
      }
    } else {
      $paged = 1;
    }
    // $paged = ($get['pg']) ? $get['pg']: 1;
  }
  $tabs = 'recent_questions';
  if(array_key_exists('tabs',$get)) {
    $tabs = $get['tabs'];
  }
  switch ($tabs) {
    case 'most_vote':
      $arg = array(
        'post_type' => 'deathnote',
        'orderby'   => 'meta_value',
        'order'     => 'desc',
        'meta_key'  => 'total_like',
        'paged' => $paged,
        'posts_per_page' => 8,
      );
      $query = new WP_Query($arg);
      break;
    case 'recently_answered':
      $limit = 8;
      $offset = ($paged - 1)*$limit;
      $querystr = "
      SELECT $wpdb->posts.* , MAX(comment_ID) as last_comment_id
      FROM $wpdb->posts
      RIGHT JOIN $wpdb->comments
      ON $wpdb->posts.ID = $wpdb->comments.comment_post_id
      WHERE $wpdb->posts.post_type = 'deathnote'
      GROUP BY ID
      ORDER BY last_comment_id DESC
      LIMIT $limit
      OFFSET $offset
      ";
      $query = $wpdb->get_results($querystr, OBJECT);
      break;
    default :
      $arg = array(
        'post_type' => 'deathnote',
        'orderby'   => 'id',
        'order'     => 'desc',
        'paged' => $paged,
        'posts_per_page' => 8,
      );
      $query = new WP_Query($arg);
  }

  return $query;
}
add_filter('list_note','action_list');

function action_search($get) {
  if(!$get) {
    $paged = 1;
  } else {
    if(array_key_exists('pg',$get)){
      if($get['pg']) {
        $paged = $get['pg'];
      } else {
        $paged = 1;
      }
    } else {
      $paged = 1;
    }
    // $paged = ($get['pg']) ? $get['pg']: 1;
  }
  $key = $get['key'];
  $args = array(
    'post_type' => 'deathnote',
    'posts_per_page' => 8,
    'search_prod_title' => $key,
    'post_status' => 'publish',
    'orderby'     => 'id',
    'order'       => 'desc',
    'paged' => $paged,
  );
  add_filter( 'posts_where', 'title_filter', 10, 2 );
  $query = new WP_Query($args);
  remove_filter( 'posts_where', 'title_filter', 10, 2 );
  return $query;
}
add_filter('list_search','action_search');
function total_page_comment(){
  global $wpdb;
  $querystr = "
  SELECT $wpdb->posts.* , MAX(comment_ID) as last_comment_id
  FROM $wpdb->posts
  RIGHT JOIN $wpdb->comments
  ON $wpdb->posts.ID = $wpdb->comments.comment_post_id
  WHERE $wpdb->posts.post_type = 'deathnote'
  GROUP BY ID
  ORDER BY last_comment_id DESC
  ";
  $query = $wpdb->get_results($querystr, OBJECT);
  $page = ceil(count($query)/8);
  return $page;
}
add_filter('get_total_pc','total_page_comment');

function action_cat($name,$get) {
  if(!$get) {
    $paged = 1;
  } else {
    $paged = ($get['pg']) ? $get['pg']: 1;
  }
  $arg = array(
    'post_type' => 'deathnote',
    'orderby'   => 'id',
    'order'     => 'desc',
    'tax_query' => array(
      array(
        'taxonomy' => 'type',
        'field' => 'slug',
        'terms' => $name
      )
    ),
    'paged' => $paged,
    'posts_per_page' => 8,
  );
  $query = new WP_Query($arg);
  return $query;
}
add_filter('notes_cate','action_cat',10,2);

function showComments($comments,$parent_id = 0,$stt = 0,$postID) {
  $comments_child = array();
  foreach($comments as $key => $item) {
    if($item->comment_parent == $parent_id) {
      $comments_child[] = $item;
      unset($comments[$key]);
    }
  }
  if($comments_child) {
    if($stt == 0) {
      $classname = "commentlist clearfix";
    }
    else if($stt > 0) {
      $classname = "children";
    }
    echo '<ul class="'.$classname.'">';
    foreach($comments_child as $key => $comment) : ?>
    <li class="comment even thread-even depth-1 comment " id="li-comment-<?php echo $comment->comment_ID?>">
      <div id="comment-<?php echo $comment->comment_ID?>" class="comment-body clearfix" rel="post-417" itemscope="" itemtype="http://schema.org/Answer">
        <div class="avatar-img">
          <?php if(!get_user_meta($comment->comment_author,'avatar',true)) : ?>
            <img alt="<?php the_author_meta('user_nicename',$comment->comment_author) ?>" class="avatar avatar-65 photo" height="65" src="https://secure.gravatar.com/avatar/345fbd70a4931ce3c0f6b85e583052a9?s=65&d=mm&r=g" srcset="https://secure.gravatar.com/avatar/345fbd70a4931ce3c0f6b85e583052a9?s=130&d=mm&r=g 2x" width="65"/>
          <?php else :?>
            <img alt="<?php the_author_meta('user_nicename',$comment->comment_author) ?>" src="<?php _e(get_user_meta($comment->comment_author,'avatar',true)['url'])?>" class="avatar avatar-79 photo" height="79" width="79">
          <?php endif;?>
        </div>
        <div class="comment-text">
          <div class="author clearfix">
            <div class="comment-author" itemprop="author" itemscope="" itemtype="http://schema.org/Person">
              <span itemprop="name"><?php echo ($comment->comment_author) ? $comment->comment_author : '匿名'; ?></span>
            </div>
            <div class="comment-vote">
              <ul class="single-question-vote">
                <li class="loader_3"></li>
                <li>
                  <a href="#" class="single-question-vote-up ask_vote_up comment_vote_up vote_allow" title="いいね！" data-comment="<?php echo $comment->comment_ID?>" id="comment_vote_up-<?php echo $comment->comment_ID?>"><i class="icon-thumbs-up"></i>
                  </a>
                </li>
              </ul>
            </div>
            <span itemprop="upvoteCount" class="question-vote-result question_vote_result"><?php _e(getlikecomment($comment->comment_ID)) ?></span>
            <div class="comment-meta" itemprop="dateCreated" datetime="2018年6月29日">
              <a href="" class="date"><i class="fa fa-calendar"></i><?php _e($comment->comment_date)?></a>
            </div>
            <div class="comment-reply">
              <?php
              $max_depth = get_option('thread_comments_depth');
              comment_reply_link(array(
                'add_below'  => 'comment',
                'respond_id' => 'respond',
                'reply_text' => __('返信する'),
                'login_text' => __(''),
                'depth'      => 1,
                'before'     => '<i class="icon-reply"></i>',
                'after'      => '',
                'max_depth'  => $max_depth
              ),$comment->comment_ID,$postID);
              ?>
            </div>
          </div>
          <div class="text">
            <div itemprop="text">
              <?php _e(nl2br($comment->comment_content))?>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="loader_3"></div>
          <div class="no_vote_more"></div>
        </div>
      </div>
      <!-- echild commnet -->
      <?php showComments($comments, $comment->comment_ID, ++$stt,$postID);?>
    </li>
    <?php endforeach;
    echo '</ul>';
  }
}

function action_releate_deathnote($id,$term_name) {
  $args = array(
    'post_type' => 'deathnote',
    'tax_query' => array(
      'taxonomy' => 'type',
      'field' => 'slug',
      'terms' => $term_name,
    ),
    'post__not_in' => array($id),
    'posts_per_page' => 5,
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('related_deathnote','action_releate_deathnote',10,2);

function action_term($post) {
  $term = wp_get_post_terms($post,'type',array("fields" => "all"));
  if(count($term) == 0){
    return $term;
  } else {
    return $term[0];
  }
}
add_filter('getterm','action_term');
?>