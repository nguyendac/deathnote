<?php get_header();?>
<body class="home">
  <div class="background-cover"></div>
  <?php get_template_part('template/template','popup')?>
  <?php get_template_part('template/template','mobile')?>
  <div class="grid_1200 boxed" id="wrap">
    <?php get_template_part('template/template','header')?>
    <!-- End section-warp -->
    <div class="index-no-box"></div>
    <div class="breadcrumbs">
      <section class="container">
        <div class="row">
          <div class="col-md-6">
            <h1>だんなと離婚したい</h1>
            <div class="clearfix"></div>
            <div class="crumbs">
              <a href="index.html">トップページ</a>
              <span class="crumbs-span">/</span>
              <span class="current">だんなと離婚したい</span>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="index-no-box"></div>
    <?php get_template_part('template/template','search')?>
    <!-- End section-warp -->
    <div class="clearfix"></div>
    <?php get_template_part('template/top/top','adver')?>
    <div class="clearfix">
    </div>
    <section class="container main-content page-right-sidebar">
      <div class="row">
        <div class="with-sidebar-container">
          <?php get_template_part('template/taxonomy/taxonomy','left')?>
          <!-- End main -->
          <?php get_template_part('template/top/top','right')?>
          <!-- End sidebar -->
          <div class="clearfix">
          </div>
        </div>
        <!-- End with-sidebar-container -->
      </div>
      <!-- End row -->
    </section>
    <!-- End container -->
    <?php get_template_part('template/template','footer')?>
  </div>
  <!-- End wrap -->
<?php get_footer();?>
