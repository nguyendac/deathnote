<?php
/*
  Template Name: login Register Page
 */

if( !session_id()){
  session_start();
}
if(get_current_user_id()){
  wp_safe_redirect(get_bloginfo('url').'/');
  exit;
}
if(isset($_POST['register'])) {
 $result = apply_filters('result_register',$_POST);
}
if(isset($_POST['login'])) {
 $loged = apply_filters('result_login',$_POST);
}
get_header();
?>
<body>
  <div class="background-cover"></div>
  <?php get_template_part('template/template','popup')?>
  <?php get_template_part('template/template','mobile')?>
  <div id="wrap" class="grid_1200 boxed">
    <?php get_template_part('template/template','header')?>
    <div class="breadcrumbs">
      <section class="container">
        <div class="row">
          <div class="col-md-12">
            <h1>デスノートを拾う(ログインor登録)</h1>
            <div class="clearfix"></div>
            <div class="crumbs">
              <a itemprop="breadcrumb" href="/">トップページ</a><span class="crumbs-span">/</span> <span class="current">デスノートを拾う(ログインor登録)</span>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class='index-no-box'></div>
    <?php get_template_part('template/template','search')?>
    <!-- End section-warp -->
    <div class="clearfix"></div>
    <?php get_template_part('template/top/top','adver')?>
    <div class="clearfix"></div>
    <section class="container main-content page-full-width">
      <div class="row">
        <div class="with-sidebar-container">
          <div class="main-sidebar-container col-md-12">
            <div class="login">
              <div class="row">
                <?php get_template_part('template/page/lr/lr','login')?>
                <?php get_template_part('template/page/lr/lr','register')?>
              </div>
              <!-- End row -->
            </div>
            <!-- End login -->
            <div class="clearfix"></div>
            <div class="advertising">
              <!-- ■ 全ページ最下部 -->
              <div class="pc">
                <a href="https://www.tantei-mr.co.jp/lp/uwaki/61.html?t=2" rel="nofollow" title="総合探偵社MR"><img src="https://danna-shine.com/cms/wp-content/uploads/2018/02/2018-02-06_728x90.jpg"></a>
              </div>
              <div class="sp">
                <a href="http://www.tantei-mr.co.jp/lp/uwaki/61.html" rel="nofollow" title="総合探偵社MR"><img src="https://danna-shine.com/cms/wp-content/uploads/2017/12/320x100-1.png"></a>
              </div>
              <!-- ■ 全ページ最下部 -->
            </div>
            <!-- End advertising -->
            <div class="clearfix"></div>
          </div>
          <!-- End main -->
          <aside class="col-md-3 sidebar"></aside>
          <!-- End sidebar -->
          <div class="clearfix"></div>
        </div>
        <!-- End with-sidebar-container -->
      </div>
      <!-- End row -->
    </section>
    <!-- End container -->
    <?php get_template_part('template/template','footer')?>
  </div>
<?php get_footer();?>